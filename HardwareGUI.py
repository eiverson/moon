# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 20:37:34 2018

@author: Erik
"""

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow
from PyQt5 import uic
 
qtCreatorFile = "HardwareGUI.ui"
 
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)
 
class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.CommsButton.pressed.connect(self.showCommsPage)
        self.MGButton.pressed.connect(self.showMGPage)
        self.OLButton.pressed.connect(self.showOLPage)
        self.PressureTempButton.pressed.connect(self.showPressureTempPage)
        
    def showCommsPage(self):
        self.stackedWidget.setCurrentIndex(0)
        self.MGButton.setChecked(False)
        self.OLButton.setChecked(False)
        self.PressureTempButton.setChecked(False)
    def showMGPage(self):
        self.stackedWidget.setCurrentIndex(1)
        self.OLButton.setChecked(False)
        self.PressureTempButton.setChecked(False)
        self.CommsButton.setChecked(False)
    def showOLPage(self):
        self.stackedWidget.setCurrentIndex(2)
        self.MGButton.setChecked(False)
        self.PressureTempButton.setChecked(False)
        self.CommsButton.setChecked(False)
    def showPressureTempPage(self):
        self.stackedWidget.setCurrentIndex(3)
        self.MGButton.setChecked(False)
        self.OLButton.setChecked(False)
        self.CommsButton.setChecked(False)        
 
if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())