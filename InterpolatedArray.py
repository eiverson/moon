# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 12:56:38 2018

@author: Erik
"""
from scipy import interpolate

class InterpolatedArray(object):

  """An array-like object that provides
  interpolated values between set points."""

  def __init__(self, points):
    self.points = sorted(points)

  def __getitem__(self, x):
    if x < self.points[0][0] or x > self.points[-1][0]:
      raise ValueError
    lower_point, upper_point = self._GetBoundingPoints(x)
    return self._Interpolate(x, lower_point, upper_point)

  def _GetBoundingPoints(self, x):
    """Get the lower/upper points that bound x."""
    lower_point = None
    upper_point = self.points[0]
    for point  in self.points[1:]:
      lower_point = upper_point
      upper_point = point
      if x <= upper_point[0]:
        break
    return lower_point, upper_point

  def _Interpolate(self, x, lower_point, upper_point):
    """Interpolate a Y value for x given lower & upper
    bounding points."""
    slope = (float(upper_point[1] - lower_point[1]) /
             (upper_point[0] - lower_point[0]))
    return lower_point[1] + (slope * (x - lower_point[0]))

if __name__ == '__main__':
    points = ((0,0), (3.31, 500), (6.28,1000 ),(9.14,1500),(12.17,2000),(14.76,2500),(15.78,3000),(16.62,3500),(17.38,4000),(18.1,4500),
              (18.8,5000),(20.07,6000),(21.25,7000),(22.48,8000),(23.55,9000),(24.3,10000),(24.35,12000),(26.1,14000),(26.45,15000))
    table = InterpolatedArray(points)
    print('testing the Interpolated Array class')
    print(table[24])
    x =[0, 3.31,6.28,9.14,12.17,14.76,15.78,16.62,17.38,18.1,18.8, 20.07,21.25, 22.48,23.55,24.3, 24.35,26.1,26.45]
    y=[0, 500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 6000,
       7000,8000, 9000,10000,12000,14000, 15000]
    f = interpolate.interp1d(x, y)
    print('testing scipy.interpolate.interp1d')
    print(f(24))
