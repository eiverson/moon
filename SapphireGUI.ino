#include "CmdMessenger.h"
#include <i2c_t3.h>
#include "ADC128D818.h" // 7 channel + temp ADC from TI
#include <ADC.h> //more capable Teensy ADC library
#include <SPI.h>
#include "ad5592_t3.h"
/* Initialize CmdMessenger -- this should match PyCmdMessenger instance */
const int BAUD_RATE = 115200;
// Attach a new CmdMessenger object to the default Serial port
char field_separator   = ',';
char command_separator = ';';
char escape_separator  = '/';

const int spi_period = 500000; //us
const int i2c_period = 10000;//us
int speed_counter = 0;
int last_value = 0;
IntervalTimer u11_timer;
IntervalTimer u12_timer;
IntervalTimer i2c_timer;

bool testState = 0; //for shunting load cell, test oscillator to tach
//PWM states
bool hga_state = 0;
bool lga_state = 0;
bool hgb_state = 0;
bool lgb_state = 0;
bool hgc_state = 0;
bool lgc_state = 0;
bool dc_relay_state = 0;
bool dc_pre_state = 0;
bool OL_Command_Toggle = 0; //for OL current or force command, IO2 on U11

//electrical quantity values (in raw bits) to be sent back to PC
unsigned int VSWs1_val, VSWs2_val, VSWs3_val, ISWs1_val, ISWs2_val, ISWs3_val = 0;
unsigned int temp_a, temp_b, temp_c, temp_hvdc, temp_e, adc_temp, imu_temp, temp_ext, teensy_temp, u11_temp, u12_temp = 0;
unsigned int tach_speed;
float raw_speed;
unsigned short fw_command, ol_command = 0 ;
unsigned int xcel_out1, xcel_out2, xcel_out3, xcel_temp = 0;
unsigned int pressure1, pressure2 = 0;
unsigned int fw_isense, ol_isense = 0;
unsigned int force, adc_force, force_err = 0; //adc_force is from adc128d818 on vacuum board

CmdMessenger c = CmdMessenger(Serial, field_separator, command_separator);
//Declare external ADC128D818 with address determined by resistors grounding pins 7 and 8.  
ADC128D818 ADC128D818(0x1D);
bool adc128d818_connected = false;

//Declare internal teensy ADC
ADC *adc = new ADC();
const int HGA = 2;
const int LGA = 3;
const int HGB = 4;
const int LGB = 5;
const int HGC = 6;
const int LGC = 7;
const int VSWs1 = 19;
const int VSWs2 = 18;
const int VSWs3 = 20;
const int ISWs1 = 62;
const int ISWs2 = 39;
const int ISWs3 = 63;
const int IBUSs1 = 37;
const int IBUSs2 = 15;
const int VBUSs1 = 34;
const int VBUSs2 = 14;
const int RSTART = 24;
const int DCMAIN = 25;
const int DCPRE = 26;
/*
 * SELA/B for controlling differential multiplexer U1, U6. 
 */
const int SELA = 22;
const int SELB = 23;
const int VBTEST = 33;

const int COM1 = 31;
const int COM2 = 32;
const int COM3 = A10;
const int COM4 = A11;
/*
 * MUXn used for demuxing SPI chip selects
 */
const int MUX1 = 29;
const int MUX2 = 28;
const int MUX3 = 27;

//ADC128D818 pin assignements (vacuum board)
const int speed_pin = 0;
const int press4 = 1;
const int press2 = 2;
const int press1 = 3;
const int press3 = 4;
const int low_speed = 5;
const int force_pin = 6;

//SPI CS pin:
const int cs = 10;
int val;
//unsigned int dac_val = 0;
SPISettings settingsA(100000, MSBFIRST, SPI_MODE2);
enum{gate_drive_a, gate_drive_b, gate_drive_c, hvdc, u12, u11};
enum{nibus, vbus, ibus, vbus2};
enum{isw, vsw, temp_a_c, temp_b_ext};
enum {
  kCommError                , // Command reports serial port comm error (only works for some comm errors)
  kComment                  , // Command to sent comment in argument
  // Setup connection test
  kAcknowledge              , // Command to acknowledge that cmd was received
  kAreYouReady              , // Command to ask if other side is ready
  kError                    , // Command to report errors
  // Acknowledge test
  kAskUsIfReady             , // Command to ask other side to ask if ready
  kYouAreReady              , // Command to acknowledge that other is ready
  askForOffloader,
  Offloader_readback,
  CmdOL, //10th command
  CmdFW,
  askForThreePhase, //12th command
  ThreePhase,
  askForDC,
  DC,//15
  askForAccel,
  Accel,
  askForPressure,
  pressure_vals,
  LowSpeed,//20th command
  askForSpeed,
  Speed,
  askForTemps,
  TemperatureValues, //24th command
  CmdTestToggle,
  CmdDCMainToggle,
  CmdDCPreToggle,
  Cmd_hga_toggle,
  Cmd_lga_toggle,
  Cmd_hgb_toggle,//30th command
  Cmd_lgb_toggle,
  Cmd_hgc_toggle,
  Cmd_lgc_toggle,
  Cmd_OL_Toggle,
  askForFW,//35
  FW_readback,
  CheckFault,
  Cmd_Config_U11,
  Cmd_Config_U12,
  Cmd_Reset_U11,//40
  Cmd_Reset_U12,
  Cmd_Config_ADC,
  Cmd_Reset_ADC,
};
void chip_select_mux(int i){
  /*
   * argument i:
   * /y3 gate drive a
   * /y0 gate drive b
   * /y1 gate drive c
   * /y2 HVDC
   * /y4 CSE U12
   * /y5 CSF U11
   */
   switch(i){
    case 0: // /yo
      digitalWrite(MUX1, LOW);
      digitalWrite(MUX2, LOW);
      digitalWrite(MUX3, LOW);
    break;
    case 1: // /y1
      digitalWrite(MUX1, HIGH);
      digitalWrite(MUX2, LOW);
      digitalWrite(MUX3, LOW);
    break;
    case 2: //  /y2
      digitalWrite(MUX1, LOW);
      digitalWrite(MUX2, HIGH);
      digitalWrite(MUX3, LOW);
    break;
    case 3: // /y3
      digitalWrite(MUX1, HIGH);
      digitalWrite(MUX2, HIGH);
      digitalWrite(MUX3, LOW);
    break;
    case 4: //   /y4
      digitalWrite(MUX1, LOW);
      digitalWrite(MUX2, LOW);
      digitalWrite(MUX3, HIGH);
    break;
    case 5:   // /y5
      digitalWrite(MUX1, HIGH);
      digitalWrite(MUX2, LOW);
      digitalWrite(MUX3, HIGH);
    break;
    default:
    break;
   }
}
void cd4052b_mux(int i){
  switch(i){
   case(0):
     digitalWrite(SELA, 0);
     digitalWrite(SELB, 0);
   case(1):
     digitalWrite(SELA, 1);
     digitalWrite(SELB, 0);    
   case(2):
     digitalWrite(SELA, 0);
     digitalWrite(SELB, 1);
   case(3):   
     digitalWrite(SELA, 1);
     digitalWrite(SELB, 1);
  }
}
void dc_relay_toggle(void){
  dc_relay_state = !dc_relay_state;
  digitalWrite(DCMAIN, dc_relay_state);
}
void dc_pre_toggle(void){
  dc_pre_state = !dc_pre_state;
  digitalWrite(DCPRE, dc_pre_state);
}
void TestToggle(void){
  testState = !testState;
  digitalWrite(VBTEST, testState);
}
void hga_toggle(void){
  hga_state = !hga_state;
  digitalWrite(HGA, hga_state);
}
void lga_toggle(void){
  lga_state = !lga_state;
  digitalWrite(LGA, lga_state);
}
void hgb_toggle(void){
  hgb_state = !hgb_state;
  digitalWrite(HGB, hgb_state);
}
void lgb_toggle(void){
  lgb_state = !lgb_state;
  digitalWrite(LGB, lgb_state);
}
void hgc_toggle(void){
  hgc_state = !hgc_state;
  digitalWrite(HGC, hgc_state);
}
void lgc_toggle(void){
  lgc_state = !lgc_state;
  digitalWrite(LGC, lgc_state);
}
void onAskForOffloader(void){
  c.sendCmdStart(Offloader_readback);
  c.sendCmdBinArg(ol_isense);
  c.sendCmdBinArg(force);
  c.sendCmdBinArg(force_err);
  c.sendCmdBinArg(adc_force);
  c.sendCmdEnd();
}
void OnArduinoReady(){
  // In response to ping. We just send a throw-away Acknowledgment to say "i'm ready"
  c.sendCmd(kAcknowledge, "Arduino ready");
}
void OnUnknownCommand(){
  // Default response for unknown commands and corrupt messages
  /*
    c.sendCmd(kError,"Unknown command");
    c.sendCmdStart(kYouAreReady);
    c.sendCmdArg("Command without attached callback");
    c.sendCmdArg(cmdMessenger.commandID());
    c.sendCmdEnd();
  */
}
void OnAskUsIfReady(){
  // The other side asks us to send kAreYouReady command, wait for
  //acknowledge
  int isAck = c.sendCmd(kAreYouReady, "Asking PC if ready", true, kAcknowledge, 1000 );
  // Now we send back whether or not we got an acknowledgments
  c.sendCmd(kYouAreReady, isAck ? 1 : 0);
}
/*
void OL_FW_Toggle(void){
  command_toggle = !command_toggle;
  chip_select_mux(u11);
  // I/O2 
  SPI.beginTransaction(settingsA);
  //SPI.transfer16(DO_WRITE_REG | (command_toggle << 2));
  SPI.endTransaction(settingsA);
}*/
void attachCallbacks(){
  c.attach(kAreYouReady, OnArduinoReady);
  c.attach(kAskUsIfReady, OnAskUsIfReady);
  c.attach(CmdTestToggle, updateTestToggle);
  c.attach(CmdDCMainToggle, dc_relay_toggle);
  c.attach(CmdDCPreToggle, dc_pre_toggle);
  c.attach(Cmd_hga_toggle, hga_toggle);
  c.attach(Cmd_lga_toggle, lga_toggle);
  c.attach(Cmd_hgb_toggle, hgb_toggle);
  c.attach(Cmd_lgb_toggle, lgb_toggle);
  c.attach(Cmd_hgc_toggle, hgc_toggle);
  c.attach(Cmd_lgc_toggle, lgc_toggle);
  c.attach(askForThreePhase, onAskForThreePhase);
  c.attach(askForDC, onAskForDC);
  c.attach(askForTemps, temperatures);
  c.attach(askForFW, onAskForFW);
  c.attach(CmdOL, onCmdOL);
  c.attach(CmdFW, onCmdFW);
  c.attach(Cmd_OL_Toggle, onCmd_OL_Toggle);
  c.attach(askForPressure, onAskForPressure);
  c.attach(askForOffloader, onAskForOffloader);
  c.attach(Cmd_Config_U11, config_u11);
  c.attach(Cmd_Config_U12, config_u12);
  c.attach(askForSpeed, onAskForSpeed);
  c.attach(Cmd_Reset_U11, reset_u11);
  c.attach(Cmd_Reset_U12, reset_u12);
  c.attach(Cmd_Config_ADC, config_ADC);
  c.attach(Cmd_Reset_ADC, reset_ADC);
}
void updateTestToggle(void){
  testState = !testState;
  digitalWrite(VBTEST, testState);
}
void onCmd_OL_Toggle(void){
  OL_Command_Toggle = !OL_Command_Toggle;
}
//LM94022 with gain select 3 is:
//2633mV at 0C
//-13.6mV / deg C
void temperatures(void){
  //temp_e and temp_d (for hvdc and pwr) are not sampled by controls test daughtercard
  //imu temp retreival not implemented yet
  //nor is adc_temp in the absense of vacuum board
  cd4052b_mux(temp_a_c);
  temp_a = adc->analogRead(COM1);
  temp_c = adc->analogRead(COM2);
  cd4052b_mux(temp_b_ext);
  temp_b = adc->analogRead(COM1);
  temp_ext = adc->analogRead(COM2);
  teensy_temp = adc->analogRead(24,1);
  c.sendCmdStart(TemperatureValues);
  c.sendCmdBinArg(temp_a);
  c.sendCmdBinArg(temp_b);
  c.sendCmdBinArg(temp_c);
  //c.sendCmdBinArg(temp_hvdc);
  //c.sendCmdBinArg(temp_e);
  
  c.sendCmdBinArg(adc_temp);
  //c.sendCmdBinArg(imu_temp);
  c.sendCmdBinArg(temp_ext);
  c.sendCmdBinArg(teensy_temp);
  c.sendCmdBinArg(u11_temp);
  c.sendCmdBinArg(u12_temp);
  c.sendCmdBinArg(xcel_temp);
  c.sendCmdEnd();
}
void onAskForPressure(void){
  c.sendBinCmd(pressure_vals, 0x00000000 | pressure1);
}
void config_u11(void){
  SPI.beginTransaction(settingsA);
  delayMicroseconds(1);
  chip_select_mux(u11);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(SOFTWARE_RESET_REG);
  digitalWrite(cs, HIGH);
  delayMicroseconds(250);
  //program U11 DACs
  digitalWrite(cs, LOW);
  val = SPI.transfer16(AO_PINS_CONFIGURATION_REG | ADPORT_CHANNEL7);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  //program U11 ADCs
  digitalWrite(cs, LOW);
  val = SPI.transfer16(AI_PINS_CONFIGURATION_REG | ADPORT_CHANNEL0 | ADPORT_CHANNEL1 | ADPORT_CHANNEL5);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  //program U11 GPIO outputs
  digitalWrite(cs, LOW);
  val = SPI.transfer16(DO_PINS_CONFIGURATION_REG | ADPORT_CHANNEL2);
  //set ADC/DAC range to 2x Vref
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(CONTROL_REG | CONTROL_REG_ADC_DAC_RANGE_2X|CONTROL_REG_ADC_BUFFER_PRECHARGE_ON);
  //turn on internal Vref
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(INTERNAL_VREF_ON);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
  u11_timer.begin(u11_timer_callback, spi_period);
  delay(50);
}
void reset_u11(void){
  SPI.beginTransaction(settingsA);
  delayMicroseconds(1);
  chip_select_mux(u11);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(SOFTWARE_RESET_REG);
  digitalWrite(cs, HIGH);
  delayMicroseconds(250);
  u11_timer.end();
}
void reset_u12(void){
  SPI.beginTransaction(settingsA);
  delayMicroseconds(1);
  chip_select_mux(u12);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(SOFTWARE_RESET_REG);
  digitalWrite(cs, HIGH);
  delayMicroseconds(250);
  u12_timer.end();
}
void config_u12(void){
    
  SPI.beginTransaction(settingsA);
  delayMicroseconds(1);
  chip_select_mux(u12);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(SOFTWARE_RESET_REG);
  digitalWrite(cs, HIGH);
  delayMicroseconds(250);
  //program U12 DACs
  digitalWrite(cs, LOW);
  val = SPI.transfer16(AO_PINS_CONFIGURATION_REG | ADPORT_CHANNEL7);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  //program U12 ADCs
  digitalWrite(cs, LOW);
  val = SPI.transfer16(AI_PINS_CONFIGURATION_REG | ADPORT_CHANNEL0 | ADPORT_CHANNEL1 |ADPORT_CHANNEL2|ADPORT_CHANNEL3|ADPORT_CHANNEL4|ADPORT_CHANNEL5|ADPORT_CHANNEL6);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  //set ADC/DAC range to 2x Vref
  digitalWrite(cs, LOW);
  val = SPI.transfer16(CONTROL_REG | CONTROL_REG_ADC_DAC_RANGE_2X|CONTROL_REG_ADC_BUFFER_PRECHARGE_ON);
  //turn on internal Vref
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  digitalWrite(cs, LOW);
  val = SPI.transfer16(INTERNAL_VREF_ON);
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
  u12_timer.begin(u12_timer_callback, spi_period);
  delay(50);
}
void config_ADC(void){
  //Start I2C for ADC128D818 and MPU9250
  Wire.begin(I2C_MASTER, 0x00, I2C_PINS_16_17, I2C_PULLUP_EXT, I2C_RATE_1000);
  Wire.setDefaultTimeout(200000); //200ms
   /* Setup ADC128D818 over I2C 
   *  
   */
  ADC128D818.setReferenceMode(EXTERNAL_REF);
  ADC128D818.setReference(5.0);
  adc128d818_connected = ADC128D818.begin();
  if (adc128d818_connected){
    i2c_timer.begin(i2c_timer_callback, i2c_period);
  }

}
void reset_ADC(void){
  i2c_timer.end();
}
void onAskForSpeed(){
  c.sendCmdStart(Speed);
  c.sendCmdBinArg(tach_speed);
  c.sendCmdBinArg(raw_speed);
  c.sendCmdEnd();

}
void setup() {
  Serial.begin(BAUD_RATE);
  pinMode(HGA, OUTPUT);
  pinMode(LGA, OUTPUT);
  pinMode(HGB, OUTPUT);
  pinMode(LGB, OUTPUT);
  pinMode(HGC, OUTPUT);
  pinMode(LGC, OUTPUT);
  pinMode(VSWs1, INPUT);
  pinMode(VSWs2, INPUT);
  pinMode(VSWs3, INPUT);
  pinMode(ISWs1, INPUT);
  pinMode(ISWs2, INPUT);
  pinMode(ISWs3, INPUT);
  pinMode(VBUSs1, INPUT);
  pinMode(VBUSs2, INPUT);
  pinMode(IBUSs1, INPUT);
  pinMode(IBUSs2, INPUT);
  pinMode(COM1, INPUT);
  pinMode(COM2, INPUT);
  pinMode(RSTART, OUTPUT);
  pinMode(DCMAIN, OUTPUT);
  pinMode(DCPRE, OUTPUT);
  pinMode(VBTEST, OUTPUT);
  pinMode(SELA, OUTPUT);
  pinMode(SELB, OUTPUT);
  pinMode(MUX1, OUTPUT);
  pinMode(MUX2, OUTPUT);
  pinMode(MUX3, OUTPUT);
  digitalWrite(VBTEST, testState);
  //Set up internal ADC for reading analogForce 
  adc->setReference(ADC_REFERENCE::REF_3V3, ADC_1);
  adc->setAveraging(16,1); // set number of averages
  adc->setResolution(12,1); // set bits of resolution to match ADC128D818
  adc->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED,1); // change the conversion speed
  adc->setSamplingSpeed(ADC_SAMPLING_SPEED::HIGH_SPEED,1); // change the sampling speed
  // always call the compare functions after changing the resolution!
  adc->enableCompare(0,1, ADC_1);
  pinMode(cs, OUTPUT);
  digitalWrite(cs, HIGH);
  SPI.begin();
  SPI.setMOSI(11);
  SPI.setMISO(12);
  SPI.setSCK(13);
  attachCallbacks();
}
void loop() {
   c.feedinSerialData();
   VSWs1_val = adc->analogRead(VSWs1);
   VSWs2_val = adc->analogRead(VSWs2);
   VSWs3_val = adc->analogRead(VSWs3);
   ISWs1_val = adc->analogRead(ISWs1);
   ISWs2_val = adc->analogRead(ISWs2);
   ISWs3_val = adc->analogRead(ISWs3); 
}

void onAskForThreePhase(void){
  c.sendCmdStart(ThreePhase);
  c.sendCmdBinArg(VSWs1_val);
  c.sendCmdBinArg(VSWs2_val);
  c.sendCmdBinArg(VSWs3_val);
  c.sendCmdBinArg(ISWs1_val);
  c.sendCmdBinArg(ISWs2_val);
  c.sendCmdBinArg(ISWs3_val);
  c.sendCmdEnd();
  
}
void onAskForDC(void){
  c.sendCmdStart(DC);
  c.sendCmdBinArg(adc->analogRead(VBUSs1));
  c.sendCmdBinArg(adc->analogRead(VBUSs2));
  c.sendCmdBinArg(adc->analogRead(IBUSs1));
  c.sendCmdBinArg(adc->analogRead(IBUSs2));
  c.sendCmdEnd();
}
void onAskForFW(void){
  c.sendBinCmd(FW_readback, 0x00000000 | fw_isense);
}
void onCmdOL(void){
  ol_command = c.readBinArg<unsigned int>();
}
void onCmdFW(void){
  fw_command = c.readBinArg<unsigned int>();
}
void u11_timer_callback(void){
  noInterrupts();
  chip_select_mux(u11);
  SPI.beginTransaction(settingsA);
  delayMicroseconds(1);
  digitalWrite(cs, LOW);
  unsigned short junk = SPI.transfer16(0x1100 | ADPORT_CHANNEL0 | ADPORT_CHANNEL1 | ADPORT_CHANNEL5 );
  //update FW command DAC
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  xcel_out2 = SPI.transfer16(0xF000 | fw_command);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  digitalWrite(cs, LOW);
  xcel_temp = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);
  digitalWrite(cs, LOW);
  force_err = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(30);
  digitalWrite(cs, LOW);
  u11_temp = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);  
  delayMicroseconds(10);
  digitalWrite(cs, LOW);
  junk = SPI.transfer16(DO_WRITE_REG | (OL_Command_Toggle ? ADPORT_CHANNEL2 : 0));
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
  interrupts();
}
void u12_timer_callback(void){
  noInterrupts();
  chip_select_mux(u12);
  SPI.beginTransaction(settingsA);
  delayMicroseconds(1);
  digitalWrite(cs, LOW);
  unsigned short junk = SPI.transfer16(0x1100 | ADPORT_CHANNEL0 | ADPORT_CHANNEL1 | ADPORT_CHANNEL2 | ADPORT_CHANNEL3 | ADPORT_CHANNEL4 | ADPORT_CHANNEL5| ADPORT_CHANNEL6);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  fw_isense = SPI.transfer16(0xF000 | ol_command);
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  force = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  xcel_out3 = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  pressure1 = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  xcel_out1 = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  pressure2 = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(10);  
  digitalWrite(cs, LOW);
  ol_isense = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  delayMicroseconds(30);  
  digitalWrite(cs, LOW);
  u12_temp  = SPI.transfer16(0x0000)& 0xFFF;
  digitalWrite(cs, HIGH);
  SPI.endTransaction();
  interrupts();
}
void i2c_timer_callback(void){
  noInterrupts();
  speed_counter += 1;
  int new_value = ADC128D818.read(low_speed);
  if ((new_value < 2000) && (last_value > 2000)){
    //an edge was seen, so update the speed
    raw_speed =   1 / (speed_counter * i2c_period * .000001 * 10);
    speed_counter =0;
  }
  last_value = new_value;
  //raw_speed = new_value;
  
  adc_temp = ADC128D818.readTemperature();
  tach_speed = ADC128D818.read(speed_pin);
  adc_force = ADC128D818.read(force_pin);
  interrupts();
}

