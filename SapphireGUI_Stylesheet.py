# -*- coding: utf-8 -*-
"""
Created on Mon Jun  4 16:05:36 2018

@author: Erik
"""
style_string = '''
        QLabel {
            background-color : #08233F;
            color : #FFAA33;
        }
        QLineEdit {
            background-color : #08233F;
            color : #FFAA33;
            border: 5px solid #FFAA33;
        }
        QPushButton#IGBT[checker=true] {
            background-color : #B8233A;
            color : #FFAA33;
            border: 5px solid #FFAA33;
            margin: 2px;
            font: italic
        }
  
        QPushButton#IGBT {
            background-color : #08233F;
            color : #FFAA33;
            border: 5px solid #FFBB33;
            margin: 2px;
            font: bold
        }

            
        QPushButton#CommButton[Connected=false] {
            background-color : #08233F;
            color : #FFAA33;
            border-radius: 7px;
            border: 7px solid #B62525;
        }
        QPushButton#CommButton[Connected=true] {
            background-color : #08233F;
            color : #FFAA33;
            border-radius: 6px;
            border: 3px solid #2FDA5F;
        }
        QPushButton#OLToggle[Force=true] {
            background-color : #E8E8DB;
            color : #FFAA33;
            border-radius: 6px;
            border: 3px solid #2FDA5F;
        }            
            
        QPushButton#OLToggle {
            background-color : #E8E8DB;
            color : #FFAA33;
            border-radius: 7px;
            border: 7px solid #B62525;
        }

        QWidget#MainWindow {
            background-color : #08233F;
        }    
            
        QLCDNumber {
            background-color : #08233F;
            color : #FFAA33;
            border: 5px solid #FFBB33;
            margin: 2px;       
        
        }
        
            
        

            '''

'''
$background: hsla(211, 77%, 14%, 1); // dark blue background, RGB 8,35,63, 0x08233F
$name-active: hsla(210, 50%, 70%, 1); // light blue text RGB 140, 179,217, 0x8CB3D9
$name-disabled: hsla(210, 50%, 70%, 0.5);
$name-passive: hsla(210, 50%, 70%, 0.15);
$value-active: hsla(35, 100%, 60%, 1); // yellow text, RGB 255, 170,51, 0xFFAA33
$value-disabled: hsla(35, 100%, 60%, 0.3);
$error-disabled: hsla(17, 90%, 45%, 0.25);
$error-active: hsl(17, 90%, 45%); // error text
$status-good: hsl(137, 70%, 52%); // green status RGB 47,218,95, 0x2FDA5F
$status-bad: hsl(0, 66%, 43%); // red status RGB 182, 37, 37, 0xB62525
'''