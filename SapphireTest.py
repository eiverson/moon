# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 11:35:36 2018

@author: Erik
"""

import sys

import PyCmdMessenger
from PyQt5.QtWidgets import QApplication
from SapphireTestGUI import HardwareGUI
from SapphireTestComm import commands, CommLink
from SapphireTestHelpers import *
from SapphireGUI_Stylesheet import style_string

app = QApplication(sys.argv)

ex = HardwareGUI()

my_Communications_Layout = Communications_Layout()
ex.addTab(my_Communications_Layout,'Communications')

my_MG_Layout = MG_Layout()
ex.addTab(my_MG_Layout, 'MG Quantities')

my_Offloader_Layout = Offloader_Layout()
ex.addTab(my_Offloader_Layout, 'Offloader')

my_PressureTemp_Layout = PressureTemp_Layout()
ex.addTab(my_PressureTemp_Layout, 'Pressure and Temp')

myCommLink = CommLink()
#connect the signals to the slots
def connect_stuff():
    myCommLink.new_Vsw1.connect(my_MG_Layout.Vsw1_LCD.display)
    myCommLink.new_Vsw2.connect(my_MG_Layout.Vsw2_LCD.display)
    myCommLink.new_Vsw3.connect(my_MG_Layout.Vsw3_LCD.display)
    
    myCommLink.new_Isw1.connect(my_MG_Layout.Isw1_LCD.display)
    myCommLink.new_Isw2.connect(my_MG_Layout.Isw2_LCD.display)
    myCommLink.new_Isw3.connect(my_MG_Layout.Isw3_LCD.display)
    
    myCommLink.new_Vdc1.connect(my_MG_Layout.Vdc1_LCD.display)
    myCommLink.new_Vdc2.connect(my_MG_Layout.Vdc2_LCD.display)
    myCommLink.new_Idc1.connect(my_MG_Layout.Idc1_LCD.display)
    myCommLink.new_Idc2.connect(my_MG_Layout.Idc2_LCD.display)
    
    myCommLink.new_FW_readback.connect(my_MG_Layout.fw_readback_LCD.display)
    myCommLink.new_ol_isense.connect(my_Offloader_Layout.current_LCD.display)
    myCommLink.new_force.connect(my_Offloader_Layout.readback_LCD.display)
    myCommLink.new_force_error.connect(my_Offloader_Layout.force_error_LCD.display)
    myCommLink.new_lca_vout.connect(my_Offloader_Layout.LCA_LCD.display)
    #myCommLink.new_adc_force.connect(my_Offloader_Layout)
    
    my_MG_Layout.igbt_sw1_button.clicked.connect(myCommLink.on_sw1_toggle)
    my_MG_Layout.igbt_sw2_button.clicked.connect(myCommLink.on_sw2_toggle)
    my_MG_Layout.igbt_sw3_button.clicked.connect(myCommLink.on_sw3_toggle)
    my_MG_Layout.igbt_sw4_button.clicked.connect(myCommLink.on_sw4_toggle)
    my_MG_Layout.igbt_sw5_button.clicked.connect(myCommLink.on_sw5_toggle)
    my_MG_Layout.igbt_sw6_button.clicked.connect(myCommLink.on_sw6_toggle)
    #my_MG_Layout.main_relay_button.clicked.connect(myCommLink.on_main_relay_toggle)
    #my_MG_Layout.main_relay_button.clicked.connect(my_MG_Layout.main_relay_button.check)
    #my_MG_Layout.pre_relay_button.clicked.connect(myCommLink.on_pre_relay_toggle)
    
    #my_MG_Layout.fw_comm_edit.editingFinished.connect(my_MG_Layout.fw_comm_edit.textChanged)
    #my_MG_Layout.fw_comm_edit.textChanged.connect(myCommLink.conditionFWcomm)
    #my_MG_Layout.fw_comm_edit.editingFinished.connect(myCommLink.on_CmdFW)
    #need to check if toggle will command current or force
    my_Offloader_Layout.force_comm_input.textChanged.connect(myCommLink.conditionOLcomm)
    my_Offloader_Layout.force_comm_input.editingFinished.connect(myCommLink.on_CmdOL)
    my_Offloader_Layout.sel_button.clicked.connect(myCommLink.onOL_commSelectToggle)
    myCommLink.changing_ol_command.connect(my_Offloader_Layout.force_comm_input.setText) #shows what is actually commanded
    myCommLink.changing_ol_command.connect(my_Offloader_Layout.sel_button.change_text) #user feedback for action
    myCommLink.changing_ol_command.connect(my_Offloader_Layout.label1.change_text) #user feedback for action
        
    
    #myCommLink.new_mg_temp.connect(my_PressureTemp_Layout.MG_LCD.display)
    #myCommLink.new_adc_temp.connect(my_PressureTemp_Layout.topcapADC_LCD.display)
    myCommLink.new_u11_temp.connect(my_PressureTemp_Layout.u11_LCD.display)
    myCommLink.new_u12_temp.connect(my_PressureTemp_Layout.u12_LCD.display)
    myCommLink.new_arm_temp.connect(my_PressureTemp_Layout.ARM_LCD.display)
    #myCommLink.new_xcel_temp.connect(my_PressureTemp_Layout.xcel_LCD.display)
    #myCommLink.new_p1.connect(my_PressureTemp_Layout.teledyne_LCD.display)
    myCommLink.new_tach_rpm_speed.connect(my_Offloader_Layout.tach_rpm_LCD.display)
    myCommLink.new_tach_rps_speed.connect(my_Offloader_Layout.tach_rps_LCD.display)
    myCommLink.new_raw_rpm_speed.connect(my_Offloader_Layout.raw_rpm_LCD.display)
    myCommLink.new_raw_rps_speed.connect(my_Offloader_Layout.raw_rps_LCD.display)    
    my_Communications_Layout.u11_connect_button.clicked.connect(myCommLink.onConfigU11)
    my_Communications_Layout.u12_connect_button.clicked.connect(myCommLink.onConfigU12)
    my_Communications_Layout.u11_disconnect_button.clicked.connect(myCommLink.onResetU11)
    my_Communications_Layout.u12_disconnect_button.clicked.connect(myCommLink.onResetU12)
    my_Communications_Layout.adc_connect_button.clicked.connect(myCommLink.onConfigADC)
    my_Communications_Layout.adc_disconnect_button.clicked.connect(myCommLink.onResetADC)
my_Communications_Layout.ComPortEdit.textEdited.connect(myCommLink.get_comm_port)
my_Communications_Layout.serial_connect_button.clicked.connect(connect_stuff)
my_Communications_Layout.serial_connect_button.clicked.connect(myCommLink.start)

app.setStyleSheet(style_string)
app.setStyle(app.style())

sys.exit(app.exec_())