# -*- coding: utf-8 -*-
"""
Created on Tue Jun 12 20:16:15 2018

@author: Erik
"""
from PyQt5.QtWidgets import QWidget, QPushButton, QSizePolicy
from PyQt5.QtGui import QIcon, QFont, QColor
from PyQt5.QtCore import Qt, QSize
class myButton(QPushButton):
    def __init__(self, icon, text, icon_size, button_size, font_size=14):
        '''
        Create a QPushButton with more detailed instantiation
        Args: 
            icon (QIcon): passed to the QPushButton __init__ 
            text (str): passed to the QPushButton __init__
            icon_size (QSize) : used as argument to setIconSize(QSize())
            button_size (QSize): used as return value for sizeHint()
        Returns:
            QPushButton: subclassed 
        
        '''
        super().__init__(icon, text)
        self.setCheckable = True
        self.setIconSize(icon_size)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum))
        self.setFont(QFont("Times", font_size))
        self.button_size = button_size
        self.setObjectName('IGBT')
      
    def sizeHint(self):
        return self.button_size

class myButton2(QPushButton):
    def __init__(self, text, font_size=18):
        '''
        Create a QPushButton with more detailed instantiation
        Args: 
       
            text (str): passed to the QPushButton __init__
            button_size (QSize): used as return value for sizeHint()
        Returns:
            QPushButton: subclassed 
        
        '''
        super().__init__(text)
        self.setCheckable = True
        self.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum))
        self.setFont(QFont("Times", font_size))
        self.button_size = QSize(150,50)
        self.setObjectName('CommButton')
        self.setProperty('Connected', False)
       
    def sizeHint(self):
        return self.button_size
 

class myButton3(QPushButton):
    def __init__(self, icon, text, icon_size, button_size, font_size=14):
        '''
        Create a QPushButton with more detailed instantiation
        Args: 
            icon (QIcon): passed to the QPushButton __init__ 
            text (str): passed to the QPushButton __init__
            icon_size (QSize) : used as argument to setIconSize(QSize())
            button_size (QSize): used as return value for sizeHint()
        Returns:
            QPushButton: subclassed 
        
        '''
        super().__init__(icon, text)
        self.setIconSize(icon_size)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum))
        self.setFont(QFont("Times", font_size))
        self.button_size = button_size
        self.text_toggler = True #true is force, false is current
        self.setProperty('checker', True)
      
    def sizeHint(self):
        return self.button_size
    def change_text(self, val): #val is unused, its here for compatibility with changing_ol_command signal
        if self.text_toggler: #if true, force is being commanded now, and we're switching to current
            self.setText('CUR')
        else:
            self.setText('FRC')
        self.text_toggler = not self.text_toggler