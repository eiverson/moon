# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 08:05:32 2018
@author: Erik


"""

#from HardwareGUI import HardwareGUI, myWindow
#import sys
#from PyQt5.QtWidgets import QApplication
import PyCmdMessenger
import time
from PyQt5.QtCore import QObject, pyqtSignal, QTimer, QThread, QEventLoop
from math import log

# List of commands and their associated argument formats. These must be in the
# same order as in the sketch.
commands = [["kCommError",""],
            ["kComment",""],
            ["kAcknowledge","s"],
            ["kAreYouReady","si"],
            ["kError","s"],
            ["kAskUsIfReady","s"],
            ["kYouAreReady","s"],
            ["askForOffloader",""],
            ["OL_readback", "IIII"], #just reading ADC128D818, not analog off board
            ["CmdOL","I"],  #10th command
            ["CmdFW","I"],
            ["askForThreePhase", ""], #12th
            ["ThreePhase", "IIIIII"], 
            ["askForDC", ""],
            ["DC", "IIII"],
            ["askForAccel", ""],
            ["Accel", "III"],
            ["askForPressure",""],
            ["pressure_vals", "I"],
            ["LowSpeed", "I"], #20th command
            ["askForSpeed", ""],
            ["Speed", "If"],
            ["askForTemps",""],
            ["TemperatureValues","IIIIIIIII"],
            ["CmdTestToggle", ""],
            ['CmdDCMainToggle',''],
            ['CmdDCPreToggle',''],
            ['Cmd_hga_toggle',''],
            ['Cmd_lga_toggle',''],
            ['Cmd_hgb_toggle',''], #30th command
            ['Cmd_lgb_toggle',''],
            ['Cmd_hgc_toggle',''],
            ['Cmd_lgc_toggle',''],
            ['Cmd_OL_Toggle','?I'],
            ['askForFW', ''],#35
            ['FW_readback', 'I'],
            ["CheckFault", ""],
            ['ConfigU11', ''],
            ['ConfigU12', ''],
            ['CmdResetU11', ''],
            ['CmdResetU12', ''],
            ['Cmd_Config_ADC', ''],
            ['Cmd_Reset_ADC', '']]
            
'''
This class wraps the PyCmdMessenger, allowing it to leverage signals and slots 
'''      
class CommLink(QThread):
    
    new_Vsw1 = pyqtSignal(float)
    new_Vsw2 = pyqtSignal(float)
    new_Vsw3 = pyqtSignal(float)
    
    new_Isw1 = pyqtSignal(float)
    new_Isw2 = pyqtSignal(float)    
    new_Isw3 = pyqtSignal(float)
    
    new_Vdc1 = pyqtSignal(float)
    new_Vdc2 = pyqtSignal(float)
    
    new_Idc1 = pyqtSignal(float)    
    new_Idc2 = pyqtSignal(float) 
  
    hga_toggle = pyqtSignal()
    lga_toggle = pyqtSignal()
    hgb_toggle = pyqtSignal()
    lgb_toggle = pyqtSignal()
    hgc_toggle = pyqtSignal()
    lgc_toggle = pyqtSignal()
    
    new_FW_readback = pyqtSignal(float)
    new_ol_isense = pyqtSignal(float) #read with Allegro ACS724
    new_adc_force = pyqtSignal(float) #read on vacuum board with adc128D818
    new_force_error = pyqtSignal(float) #buffered current command to LT3741
    new_force = pyqtSignal(float) #read from AD5592 on controls board
    new_lca_vout = pyqtSignal(float)
    changing_ol_command = pyqtSignal(str)
    
    new_gda_temp = pyqtSignal(float)
    new_gdb_temp = pyqtSignal(float)
    new_gdc_temp = pyqtSignal(float)
    #new_hvdc_temp = pyqtSignal(float)
    #new_pwr_temp = pyqtSignal(float)
    new_adc_temp = pyqtSignal(float)
    #new_imu_temp pyqtSignal(float)
    new_mg_temp = pyqtSignal(float)
    new_arm_temp = pyqtSignal(float)
    new_u11_temp = pyqtSignal(float)
    new_u12_temp = pyqtSignal(float)
    new_xcel_temp = pyqtSignal(float)
    new_p1 = pyqtSignal(float)
    
    new_tach_rpm_speed = pyqtSignal(float)
    new_tach_rps_speed = pyqtSignal(float)
    new_raw_rpm_speed = pyqtSignal(float)
    new_raw_rps_speed = pyqtSignal(float)
    
    def __init__(self, *args, **kwargs):
        QThread.__init__(self)
        #super().__init__()
        self.myPyCmdMessenger = 0
        self.timer = QTimer()
        self.timer.setInterval(800) #mSec
        self.timer.moveToThread(self)
        self.timer.timeout.connect(self.update_request)
        self.fwComm = 0
        self.OLComm = 0
        #true is force command, false is current command
        self.commanding_force = True 
        self.OL_current = 0
        self.OL_force = 0
        self.OL_force_bits = 0
        self.lca_sns = .015
        self.lca_gain = 100
        self.lca_max = 1000
        self.com_port = 0;
    def conditionFWcomm (self, str):
        if not str:
            self.fwComm = 0
        else:
            self.fwComm = int(str)
            
    def conditionOLcomm(self, str):
        if not str:
            self.OLComm = 0
        else:
            try:
                self.OLComm = int(str)
            except ValueError:
                self.OLComm = 0

    def run(self):
        
        arduino = PyCmdMessenger.ArduinoBoard('COM{}'.format(self.com_port),
             baud_rate=115200,
             timeout=1.0,
             settle_time=2.0,
             enable_dtr=False,
             int_bytes=4,
             long_bytes=4,
             float_bytes=4,
             double_bytes=8) 
        self.myPyCmdMessenger = PyCmdMessenger.CmdMessenger(arduino,commands)
        time.sleep(2)
        self.timer.start()
        loop = QEventLoop()
        loop.exec_()
    def get_comm_port(self, port_num):
        self.com_port = int(port_num)
    def update_request(self):
        '''
        fetch new values from PyCmdMessenger. 
        emit the signals that are connected to the GUI Widgets' slots 
        '''
        '''
        self.myPyCmdMessenger.send("askForThreePhase")
        vals = self.myPyCmdMessenger.receive()[1]
        self.new_Vsw1.emit(vals[0]*(3.3/4096))
        self.new_Vsw2.emit(vals[1]*(3.3/4096))
        self.new_Vsw3.emit(vals[2]*(3.3/4096))
        self.new_Isw1.emit(vals[3]*(3.3/4096))
        self.new_Isw2.emit(vals[4]*(3.3/4096))
        self.new_Isw3.emit(vals[5]*(3.3/4096))
      
        self.myPyCmdMessenger.send("askForDC")
        vals = self.myPyCmdMessenger.receive()[1]
        self.new_Vdc1.emit(vals[0]*(3.3/4096))
        self.new_Vdc2.emit(vals[1]*(3.3/4096))
        self.new_Idc1.emit(vals[2]*(3.3/4096))
        self.new_Idc2.emit(vals[3]*(3.3/4096))
        '''
        #self.myPyCmdMessenger.send("askForFW")
        #vals = self.myPyCmdMessenger.receive()[1]
        #self.new_FW_readback.emit((vals[0]*5/4096 - .5)/.2)#ACS724 sensor
      
        
        self.myPyCmdMessenger.send("askForTemps")
        vals = self.myPyCmdMessenger.receive()[1]
        self.new_gda_temp.emit((vals[0]*(3.3/4096)-2.633)/(-.0136)) #lm94022 sensor
        self.new_gdb_temp.emit((vals[1]*(3.3/4096)-2.633)/(-.0136))
        self.new_gdc_temp.emit((vals[2]*(3.3/4096)-2.633)/(-.0136))
        self.new_adc_temp.emit(round(self.calc_adc_temp(vals[3]),1)) #ADC128D818 datasheet
        self.new_mg_temp.emit(round(self.calc_MG_temp(vals[4]),1))
        self.new_arm_temp.emit(round(((vals[5]*3.3/4096)-.716)/(-.00162) + 25,1))
        self.new_u11_temp.emit(round( (vals[6]-410)/2.654 + 25,1)) #ad5592 data sheet
        self.new_u12_temp.emit(round((vals[7]-410)/2.654 + 25,1))      
        self.new_xcel_temp.emit(round(((vals[8]*5/4096)-.8922)/.003 + 25,1))
        #print(vals[8]*5/4096)#ADXL354 
        
        #self.myPyCmdMessenger.send("askForPressure")
        #vals = self.myPyCmdMessenger.receive()[1]
        #self.new_p1.emit(vals[0]*5/4096)
        
        self.myPyCmdMessenger.send("askForOffloader")
        vals = self.myPyCmdMessenger.receive()[1]
        
        self.OL_current = (vals[0]*5/4096 - .5)/.2 #for transitioning from force to current command on the fly if necessary
        self.new_ol_isense.emit(self.OL_current)#ACS724 sensor
        self.OL_force_bits = vals[1]
        self.OL_force = round(vals[1]*(5/4096)*(self.lca_max/self.lca_sns)*(1/self.lca_gain),0)#1000lb/.015V, LCA Gain = 100
        self.new_force.emit(self.OL_force)#1000lb/.015V, LCA Gain = 100
        self.new_force_error.emit((vals[2]*5/4096)/.2)
        self.new_adc_force.emit(round(vals[3]*(5/4096)*(self.lca_max/self.lca_sns)*(1/self.lca_gain),0))#1000lb/.015V, LCA Gain = 100
        self.new_lca_vout.emit(vals[3]*(5/4096))
        
        self.myPyCmdMessenger.send("askForSpeed")
        vals = self.myPyCmdMessenger.receive()[1]
        self.new_tach_rps_speed.emit(vals[0]*(5/4096)*(110/5))#110Hz at 5V on LM2917
        self.new_tach_rpm_speed.emit(vals[0]*(5/4096)*(110/5)*60)
        self.new_raw_rps_speed.emit(vals[1])
        self.new_raw_rpm_speed.emit(vals[1]*60)
    def calc_adc_temp(self,bits):
        if ((bits & 0x100) == 0):
            return float(bits / 2);
        else:
            return float(-1*(512 - bits) / 2);

    def calc_MG_temp(self, bits):
        a = 0.00335401643468053
        b = 0.000256523550896126
        c = 2.60597012072052E-06
        d = 6.32926126487455E-08
        mg_temp_V = bits*3.3/4096
        Rratio = (mg_temp_V/(3.3-mg_temp_V))
        return (1 / (a + b*log(Rratio) + c*log(Rratio)**2 + d*log(Rratio)**3)) - 273.15
        
    def on_Test_Toggle(self):
        self.myPyCmdMessenger.send("CmdTestToggle")
        time.sleep(.1)
    def onOL_commSelectToggle(self):
        '''
        before toggling the command, make current matches what it is at now,
        to  make sure the transition is smooth.  
        '''
        
        
        if self.commanding_force: #going from force to current
            new_current = int(7*(1/5)*(4096/5))#go straight to 7A
            self.myPyCmdMessenger.send("Cmd_OL_Toggle", False, new_current)#sending false means we want to command current
            vals = self.myPyCmdMessenger.receive()[1]
            if not vals[0]: #received false back indicating a successful transition to current command
                self.OLcomm = new_current #go straight to 7A
                self.changing_ol_command.emit(str(round(7,0))) #updates the command LineEdit, the button, and the command 
                self.commanding_force= not self.commanding_force
        else: #going from current to force. Use quiescent force value + 100lb to be safe
            new_force = self.OL_force_bits + 100 #the latest force bits from the last update request
            self.myPyCmdMessenger.send("Cmd_OL_Toggle", True, new_force)#sending true means we want to command force
            vals = self.myPyCmdMessenger.receive()[1]
            if vals[0]:    
                self.OLcomm = new_force
                self.changing_ol_command.emit(str(round(new_force*(5/4096)*(self.lca_max/self.lca_sns)*(1/self.lca_gain),0)))
                self.commanding_force = not self.commanding_force
        
        
    def on_main_relay_toggle(self):
        self.myPyCmdMessenger.send("CmdDCMainToggle")
        #self.
    def on_pre_relay_toggle(self):
        self.myPyCmdMessenger.send("CmdDCPreToggle")
    def on_sw1_toggle(self):
        self.myPyCmdMessenger.send("Cmd_hga_toggle")
    def on_sw2_toggle(self):
        self.myPyCmdMessenger.send("Cmd_lga_toggle")
    def on_sw3_toggle(self):
        self.myPyCmdMessenger.send("Cmd_hgb_toggle")
    def on_sw4_toggle(self):
        self.myPyCmdMessenger.send("Cmd_lgb_toggle")
    def on_sw5_toggle(self):
        self.myPyCmdMessenger.send("Cmd_hgc_toggle")
    def on_sw6_toggle(self):
        self.myPyCmdMessenger.send("Cmd_lgc_toggle")     
    def on_CmdFW(self):
        self.myPyCmdMessenger.send("CmdFW", int(self.fwComm*(1/5)*(4096/5)))#5A/V
    def on_CmdOL(self):
        '''
        
        '''
        #self.myPyCmdMessenger.send("Cmd_OL_Toggle")
        if self.commanding_force:
            self.myPyCmdMessenger.send("CmdOL", int(self.OLComm/((5/4096)*(self.lca_max/self.lca_sns)*(1/self.lca_gain))))#1000lb/.015V, LCA Gain = 100 )
        else: 
            self.myPyCmdMessenger.send("CmdOL", int(self.OLComm*(1/5)*(4096/5)*(1.5/5)))#1V/5A (no resistive divider)
    def onConfigU11(self):
        self.myPyCmdMessenger.send("ConfigU11")
    def onConfigU12(self):
        self.myPyCmdMessenger.send("ConfigU12")
    def onResetU11(self):
        self.myPyCmdMessenger.send("CmdResetU11")
    def onResetU12(self):
        self.myPyCmdMessenger.send("CmdResetU12")
    def onConfigADC(self):
        self.myPyCmdMessenger.send("Cmd_Config_ADC")
    def onResetADC(self):
        self.myPyCmdMessenger.send("Cmd_Reset_ADC")
        
        
if __name__ == '__main__':                
    # Initialize the messenger
    arduino = PyCmdMessenger.ArduinoBoard('COM30',
         baud_rate=115200,
         timeout=1.0,
         settle_time=2.0,
         enable_dtr=False,
         int_bytes=4,
         long_bytes=4,
         float_bytes=4,
         double_bytes=8) 
    #c = PyCmdMessenger.CmdMessenger()
    #c = CommLink()
    c= PyCmdMessenger.CmdMessenger(arduino,commands)
    
    # Check connection
    '''
    c.send("kAreYouReady")
 
    # Clear welcome message etc. from buffer
    i = 0
    success = False
    while i < 3:
        value = c.receive()
        time.sleep(0.2)
        if value != None:
            success = True
        i += 1
  
    # Make sure we're connected 
    if not success:
        err = "Could not connect."
        raise Exception(err)
    '''
    c.send("askForThreePhase")
    #print(vals)
    vals = c.receive()[1]
    print("return from Arduino:")
    #print(vals)
    print("individual values:")
    print(vals[0]*(3.3/4096))
    print(vals[1]*(3.3/4096))
    print(vals[2]*(3.3/4096))
    print(vals[3]*(3.3/4096))
    print(vals[4]*(3.3/4096))
    print(vals[5]*(3.3/4096))
  
    c.send("askForDC")
    vals = c.receive()[1]
    print(vals[0]*(5/4096))
    print(vals[1]*(5/4096))
    print(vals[2]*(5/4096))
    print(vals[3]*(5/4096))
    
    print('testing OL toggle')
#going from force to current
    new_current = 7*(1/5)*(4096/5)#go straight to 7A
    c.send("Cmd_OL_Toggle", False, new_current)#sending false means we want to command current
    vals = c.receive()
    print(vals)
    if not vals[0]: #received false back indicating a successful transition to current command
        print('force to current works')
         #going from current to force. Use quiescent force value + 100lb to be safe
    new_force =   100 #the latest force bits from the last update request
    c.send("Cmd_OL_Toggle", True, new_force)#sending true means we want to command force
    vals = c.receive()[1]
    print(vals)
    if vals[0]:    
        print("current to force works")
    for i in range(0, 4096, 100):
        c.send("CmdFW", i)#5A/V)
        time.sleep(.2)
    print('that is all for now')
    
    