# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 11:36:54 2018

@author: Erik
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QMainWindow, QAction, qApp,
    QLCDNumber, QGridLayout, QLabel, QVBoxLayout, QHBoxLayout, QSizePolicy, QScrollArea)
from PyQt5.QtGui import QIcon, QFont, QColor, QPalette, QPixmap
from PyQt5.QtCore import Qt, QSize
from SapphireTestHelpers import *
from SapphireGUI_Stylesheet import style_string

class HardwareGUI(QMainWindow):
    
    #def __init__(self, *args, **kwargs):
     #   super().__init__()
    def __init__(self):   
        super(QMainWindow, self).__init__()
        #self.setObjectName('MainWindow')
        '''
        scrollArea is made the central widget of this QMainWIndow, and 
        becomes a child of it. 
        scrollAreaWidgetContents is a QWidget child of scrollArea, and supposedly 
        it must be unique. It manages the scrollbars when they are needed. 
        '''
        self.scrollArea = QScrollArea()
        
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget(self.scrollArea)
        '''Documentation states the argument of scrollArea.setWidget() must have its
        layout set already. 
        '''
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        #this next line determines when the scroll bars appear
        self.scrollAreaWidgetContents.setMinimumSize(QSize(1100, 1300)) 
        self.setCentralWidget(self.scrollArea)
        #self.scrollAreaWidgetContents.setObjectName('MainWindow')
        self.table_layout = QVBoxLayout(self.scrollAreaWidgetContents) 
        self.table_widget = MyTableWidget(self)
        #self.table_widget.setObjectName('MainWindow')
        self.tabs = []

        self.table_layout.addWidget(self.table_widget)    
         
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)

        exitAct = QAction(QIcon('exit2.png'), '&Exit', self)        
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.centralWidget().close)
        exitAct.triggered.connect(qApp.closeAllWindows)        
        
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)
        self.statusBar().showMessage('Ready')
        self.setWindowTitle('Amber Kinetics Controls Board GUI')  
        self.setWindowIcon(QIcon('A.png'))
        self.setGeometry(200, 200, 1400, 700)
        self.show()
        
    def addTab(self, tab_layout, tab_title):
        new_tab = QWidget()
        new_tab.setLayout(tab_layout)
        self.tabs.append(new_tab)
        self.table_widget.addTab(new_tab, tab_title)
    def styleToggler(self, widget):
        isChecked = widget.property('checker')
        widget.setProperty('checker', not isChecked)
        widget.setStyle(widget.style())
    def toggle(self, widget):
        isChecked = widget.property('checker')
        widget.setProperty('checker', not isChecked)
        widget.setStyle(widget.style())


            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyleSheet(style_string)
    app.setStyle(app.style())
    ex = HardwareGUI()
    my_Communications_Layout = Communications_Layout()
    ex.addTab(my_Communications_Layout,'Communications')
    my_MG_Layout = MG_Layout()
    ex.addTab(my_MG_Layout, 'MG Quantities')
    ex.addTab(Offloader_Layout(), 'Offloader')
    ex.addTab(PressureTemp_Layout(), 'Pressure and Temp')
    my_MG_Layout.igbt_sw1_button.clicked.connect(lambda: ex.styleToggler(my_MG_Layout.igbt_sw1_button))
    
    sys.exit(app.exec_())
         
'''
$background: hsla(211, 77%, 14%, 1); // dark blue background, RGB 8,35,63, 0x08233F
$name-active: hsla(210, 50%, 70%, 1); // light blue text RGB 140, 179,217, 0x8CB3D9
$name-disabled: hsla(210, 50%, 70%, 0.5);
$name-passive: hsla(210, 50%, 70%, 0.15);
$value-active: hsla(35, 100%, 60%, 1); // yellow text, RGB 255, 170,51, 0xFFAA33
$value-disabled: hsla(35, 100%, 60%, 0.3);
$error-disabled: hsla(17, 90%, 45%, 0.25);
$error-active: hsl(17, 90%, 45%); // error text
$status-good: hsl(137, 70%, 52%); // green status RGB 47,218,95, 0x2FDA5F
$status-bad: hsl(0, 66%, 43%); // red status RGB 182, 37, 37, 0xB62525
'''
    