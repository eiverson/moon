# -*- coding: utf-8 -*-
"""
Created on Wed May  2 21:53:23 2018

@author: Erik
"""

from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QMainWindow, QAction, qApp,
    QLCDNumber, QLabel, QSizePolicy, QScrollArea, QLayout, QLineEdit, QVBoxLayout,
    QTabWidget, QGridLayout, QHBoxLayout, QSlider)
from PyQt5.QtGui import QIcon, QFont, QColor, QPalette, QPixmap, QIntValidator
from PyQt5.QtCore import Qt, QSize
from SapphireTestButtons import *

class myLCD(QLCDNumber):
    def __init__(self, size):
        super().__init__()
        self.setSizePolicy(size)
        self.setSmallDecimalPoint(True)
       
    def sizeHint(self):
        return QSize(200,75)

class myLabel(QLabel):
    def __init__(self, size, text, font_size=22):
        super().__init__(text)
        self.setSizePolicy(size)
        self.setFont(QFont("Times",font_size))
        self.setAlignment(Qt.AlignCenter)
        
    def sizeHint(self):
        return QSize(200, 75)
    
class myLabel2(QLabel):
    def __init__(self, size, text, font_size=22):
        super().__init__(text)
        self.setSizePolicy(size)
        self.setFont(QFont("Times",font_size))
        self.setAlignment(Qt.AlignCenter)
        self.commanding_force = True
    def sizeHint(self):
        return QSize(200, 75)
    def change_text(self,val):#val is unused, its here for compatibility with changing_ol_command signal
        if self.commanding_force:
            self.setText('Current Command [A]')
            
        else:
            self.setText('Force Command [lbs]')
        self.commanding_force = not self.commanding_force
    
class myLineEdit(QLineEdit):
    def __init__(self, size):
        super().__init__()
        self.setSizePolicy(size)
        font = self.font()
        font.setPointSize(24)
        self.setFont(font)
        #self.setValidator(QIntValidator(0,2000))
        
    def sizeHint(self):
        return QSize(90,75)
    
class MyTableWidget(QWidget):
    def __init__(self, parent):   
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout(self)
        # Initialize tab screen
        self.tabs = QTabWidget()      
        self.layout.addWidget(self.tabs)
    
    def addTab(self, widget, text):
        self.tabs.addTab(widget, text)

class MG_Layout(QVBoxLayout):
    def __init__(self):
        super().__init__()
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)
        
    
        
        '''      
        my_yellow = QColor.fromHsl(35, 255, 153, 128 )
        dark_blue = QColor.fromHsl(211, 196, 38, 255)
        self.setAutoFillBackground(True)
        palette = self.palette()
        palette.setColor(QPalette.Window, dark_blue)
        palette.setColor(QPalette.WindowText, my_yellow)
        self.setPalette(palette)
        '''
        self.top_hbox = QHBoxLayout()
        
        self.addLayout(self.top_hbox)
        self.top_hbox.addWidget(self.AKlabel)
        self.top_hbox.addWidget(myLabel(size, '    MG Quantities', 26))
        self.top_hbox.insertStretch(-1)
        self.grid = QGridLayout()
        
        self.addLayout(self.grid)
        
        self.Vsw1_label = myLabel(size, 'Vsw1 [V]')
        self.Vsw2_label = myLabel(size, 'Vsw2 [V]')
        self.Vsw3_label = myLabel(size, 'Vsw3 [V]')
        self.Isw1_label = myLabel(size, 'Isw1 [A]')
        self.Isw2_label = myLabel(size, 'Isw2 [A]')
        self.Isw3_label = myLabel(size, 'Isw3 [A]')
        
        self.Vsw1_LCD = myLCD(size)
        self.Vsw2_LCD = myLCD(size)
        self.Vsw3_LCD = myLCD(size)
        self.Isw1_LCD = myLCD(size)
        self.Isw2_LCD = myLCD(size)
        self.Isw3_LCD = myLCD(size)
         
        self.grid.addWidget(self.Vsw1_label, 0,0, Qt.AlignLeft )
        self.grid.addWidget(self.Vsw2_label, 1,0 , Qt.AlignLeft)
        self.grid.addWidget(self.Vsw3_label, 2,0 , Qt.AlignLeft)
        self.grid.addWidget(self.Vsw1_LCD, 0, 1, Qt.AlignLeft)
        self.grid.addWidget(self.Vsw2_LCD, 1, 1, Qt.AlignLeft)
        self.grid.addWidget(self.Vsw3_LCD, 2, 1, Qt.AlignLeft)
        
        self.grid.addWidget(self.Isw1_label, 0,2 , Qt.AlignLeft)
        self.grid.addWidget(self.Isw2_label, 1,2, Qt.AlignLeft )
        self.grid.addWidget(self.Isw3_label, 2,2 , Qt.AlignLeft)
        self.grid.addWidget(self.Isw1_LCD, 0, 3, Qt.AlignLeft)
        self.grid.addWidget(self.Isw2_LCD, 1, 3, Qt.AlignLeft)
        self.grid.addWidget(self.Isw3_LCD, 2, 3, Qt.AlignLeft)
        
        self.Vdc1_label = myLabel(size, 'Vdc1 [V]')
        self.Vdc2_label = myLabel(size, 'Vdc2 [V]')
        self.Idc1_label = myLabel(size, 'Idc1 [V]')
        self.Idc2_label = myLabel(size, 'Idc2 [V]')
        
        self.Vdc1_LCD = myLCD(size)
        self.Vdc2_LCD = myLCD(size)
        self.Idc1_LCD = myLCD(size)
        self.Idc2_LCD = myLCD(size)
        
        self.fw_comm_label = myLabel(size,'Field Winding Command [A]', 20)
        self.fw_readback_label = myLabel(size, 'Field Winding Readback [A]',20)
        self.fw_comm_edit = myLineEdit(size)
        self.fw_comm_edit.setValidator(QIntValidator(0,25))
        self.fw_comm_edit.setText('0')
        #self.fw_comm_edit.setText(0)
        self.fw_readback_LCD = myLCD(size)
        
        self.grid.addWidget(self.Vdc1_label, 3, 0, Qt.AlignLeft)
        self.grid.addWidget(self.Vdc2_label, 4, 0, Qt.AlignLeft)
        self.grid.addWidget(self.Idc1_label, 3, 2, Qt.AlignLeft)
        self.grid.addWidget(self.Idc2_label, 4, 2, Qt.AlignLeft)
        self.grid.addWidget(self.Vdc1_LCD, 3, 1, Qt.AlignLeft)
        self.grid.addWidget(self.Vdc2_LCD, 4, 1, Qt.AlignLeft)
        self.grid.addWidget(self.Idc1_LCD, 3, 3, Qt.AlignLeft)
        self.grid.addWidget(self.Idc2_LCD, 4,3, Qt.AlignLeft) 
        
        self.grid.addWidget(self.fw_comm_label, 5,0,1,2, Qt.AlignLeft)
        self.grid.addWidget(self.fw_comm_edit, 5,2, Qt.AlignLeft)
        self.grid.addWidget(self.fw_readback_label, 5,3,1,2, Qt.AlignLeft)
        self.grid.addWidget(self.fw_readback_LCD, 5,5, Qt.AlignLeft)
        
        #self.main_relay_button = myRelayButton(QIcon('relay.PNG'), 'Main')
        #self.pre_relay_button = myRelayButton(QIcon('relay.PNG'), 'Pre')
        self.main_relay_button = myButton(QIcon('relay.PNG'), 'Main', QSize(180,65), QSize(190,70))
        self.pre_relay_button = myButton(QIcon('relay.PNG'), 'Pre', QSize(180,65), QSize(190,70)) 
        self.grid.addWidget(self.main_relay_button, 3,4, Qt.AlignLeft)
        self.grid.addWidget(self.pre_relay_button, 4,4, Qt.AlignLeft)
        
        self.igbt_sw1_button= myButton(QIcon('igbt.png'), 'SW1', QSize(70,70), QSize(160,100))
        self.igbt_sw2_button= myButton(QIcon('igbt.png'), 'SW2', QSize(70,70), QSize(160,100))
        self.igbt_sw3_button =myButton(QIcon('igbt.png'), 'SW3', QSize(70,70), QSize(160,100))
        self.igbt_sw4_button =myButton(QIcon('igbt.png'), 'SW4', QSize(70,70), QSize(160,100))
        self.igbt_sw5_button= myButton(QIcon('igbt.png'), 'SW5', QSize(70,70), QSize(160,100))
        self.igbt_sw6_button =myButton(QIcon('igbt.png'), 'SW6', QSize(70,70), QSize(160,100))
        
        self.igbt_sw1_button.setObjectName('IGBT')
        
        
        sw_grid = QGridLayout()
        sw_grid.addWidget( self.igbt_sw1_button,0,0)
        sw_grid.addWidget( self.igbt_sw2_button,1,0)
        sw_grid.addWidget( self.igbt_sw3_button,0,1)
        sw_grid.addWidget( self.igbt_sw4_button,1,1)
        sw_grid.addWidget( self.igbt_sw5_button,0,2)
        sw_grid.addWidget( self.igbt_sw6_button,1,2)
        
        self.grid.addLayout(sw_grid, 0,4,3,3, Qt.AlignLeft)
   
        self.grid.setColumnStretch(0,0)
        self.grid.setColumnStretch(1,0)
        self.grid.setColumnStretch(2,0)
        self.grid.setColumnStretch(3,0)
        self.grid.setColumnStretch(4,0)
        self.grid.setColumnStretch(5,0)
        self.grid.setColumnStretch(5,2)
   
        self.insertStretch(-1)

class Offloader_Layout(QVBoxLayout):
    def __init__(self):
        super().__init__()
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)
        
        self.top_hbox = QHBoxLayout()
        
        self.top_hbox.addWidget(self.AKlabel, Qt.AlignLeft)
        self.top_hbox.addWidget(myLabel(size, '  Offloader and Speed Quantities', 24))
        self.top_hbox.insertStretch(-1)
        self.grid = QGridLayout()
        self.addLayout(self.top_hbox)
        self.addLayout(self.grid)
        
        #self.force_label = myLabel(size, 'Bearing Force', 24)
        self.label1 = myLabel2(size, 'Force Command [lbs]', 18)
        self.label2 = myLabel(size, 'Readback [lbs]', 20)
        self.label3 = myLabel(size, 'LCA Vout [V]', 20)
        self.label4 = myLabel(size, 'Current [A]', 20)
        self.label5 = myLabel(size, 'Current Command [A]', 18)
        self.label6 = myLabel(size, 'Force Error [A]', 20)
        self.label7 = myLabel(size, 'OL Comm Readback [V]',18)
        self.label8 = myLabel(size, 'Speed [RPS]')
        self.label9 = myLabel(size, 'Speed [RPM]')
        self.label10 = myLabel(size, 'Tachometer', 24)
        self.label11 = myLabel(size, 'Raw Sensor Sample', 24)
        self.label12 = myLabel(size, 'Speed [RPS]')
        self.label13 = myLabel(size, 'Speed [RPM]')
        self.force_comm_input = myLineEdit(size)
        self.force_comm_input.setValidator(QIntValidator(0,5000))
        self.force_comm_input.setText('5000')
        self.i_comm_input =  myLineEdit(size)
        self.i_comm_input.setValidator(QIntValidator(0,30))
        self.i_comm_input.setText('0')
        self.sel_button = myButton3(QIcon('ol_select.png'), 'FRC', QSize(70,70), QSize(160,100))
        self.test_button = myButton(QIcon('test.png'), '', QSize(70,70), QSize(90,90))
        self.readback_LCD = myLCD(size)
        self.LCA_LCD = myLCD(size)
        self.current_LCD = myLCD(size)
        self.force_error_LCD = myLCD(size)
        self.tach_rpm_LCD = myLCD(size)
        self.tach_rps_LCD = myLCD(size)
        self.raw_rpm_LCD = myLCD(size)
        self.raw_rps_LCD = myLCD(size)
        
        
        self.grid.addWidget(self.sel_button, 0,0)
        self.grid.addWidget(self.test_button,0,2)
        self.grid.addWidget(self.label1, 1,0, Qt.AlignLeft)
        self.grid.addWidget(self.force_comm_input, 1,1, Qt.AlignLeft)
        #self.grid.addWidget(self.label5,2,0, Qt.AlignLeft)
        #self.grid.addWidget(self.i_comm_input,2,1, Qt.AlignLeft )
        self.grid.addWidget(self.label2, 1,2, Qt.AlignLeft)
        self.grid.addWidget(self.label3, 2,2, Qt.AlignLeft)
        self.grid.addWidget(self.label4, 3,2, Qt.AlignLeft)
        self.grid.addWidget(self.label6, 4,2, Qt.AlignLeft)
        self.grid.addWidget(self.readback_LCD, 1,3, Qt.AlignLeft)
        self.grid.addWidget(self.LCA_LCD, 2, 3, Qt.AlignLeft)
        self.grid.addWidget(self.current_LCD, 3,3, Qt.AlignLeft)
        self.grid.addWidget(self.force_error_LCD,4,3, Qt.AlignLeft)
        
        self.grid.addWidget(self.label10, 0,4,1,2, Qt.AlignCenter)
        self.grid.addWidget(self.label8, 1,4, Qt.AlignLeft)
        self.grid.addWidget(self.label9, 2,4, Qt.AlignLeft)
        self.grid.addWidget(self.label11,3,4,1,2, Qt.AlignCenter)
        self.grid.addWidget(self.label12, 4,4, Qt.AlignLeft)
        self.grid.addWidget(self.label13, 5,4, Qt.AlignLeft)
        
        self.grid.addWidget(self.tach_rps_LCD, 1,5, Qt.AlignLeft)
        self.grid.addWidget(self.tach_rpm_LCD, 2,5, Qt.AlignLeft)   
        self.grid.addWidget(self.raw_rps_LCD, 4,5, Qt.AlignLeft)
        self.grid.addWidget(self.raw_rpm_LCD, 5,5, Qt.AlignLeft)        
        self.grid.setColumnStretch(0,0)
        self.grid.setColumnStretch(1,0)
        self.grid.setColumnStretch(2,0)
        self.grid.setColumnStretch(3,0)
        self.grid.setColumnStretch(4,0)
        self.grid.setColumnStretch(5,2)
        
        self.insertStretch(-1)

class PressureTemp_Layout(QVBoxLayout):
    def __init__(self):
        super().__init__()
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)
        
        self.top_hbox = QHBoxLayout()
        self.top_hbox.addWidget(self.AKlabel, Qt.AlignLeft)
        self.top_hbox.addWidget(myLabel(size, '    Pressure [mTorr] and Temperature [C]', 24))
        self.top_hbox.insertStretch(-1)
        self.grid = QGridLayout()
        self.addLayout(self.top_hbox)
        self.addLayout(self.grid)
        
        self.label1 = myLabel(size, 'GD A', 20)
        self.label2 = myLabel(size, 'GD B', 20)
        self.label3 = myLabel(size, 'GD C', 20)
        self.label4 = myLabel(size, 'HVDC', 20)
        self.label5 = myLabel(size, 'PWR Board', 20)
        self.label6 = myLabel(size, 'Top Cap ADC', 20)
        self.label7 = myLabel(size, 'Top Cap IMU', 20)
        self.label8 = myLabel(size, 'MG ext', 20)
        self.label9 = myLabel(size, 'ARM uC', 20)  
        self.label10 = myLabel(size, 'AD5592 U11', 20)
        self.label11 = myLabel(size, 'AD5592 U12', 20)
        
        self.label12 = myLabel(size, 'Teledyne', 20)
        self.label13= myLabel(size, 'Posifa', 20)
        self.label14 = myLabel(size, 'Fan PWM', 20)
        self.label15 = myLabel(size, 'XCEL', 20)
         
        self.GDA_LCD = myLCD(size)
        self.GDB_LCD = myLCD(size)
        self.GDC_LCD = myLCD(size)
        self.HVDC_LCD = myLCD(size)
        self.PWR_LCD = myLCD(size)
        self.topcapADC_LCD = myLCD(size)
        self.topcapIMU_LCD = myLCD(size)
        self.MG_LCD = myLCD(size)
        self.ARM_LCD = myLCD(size)
        self.u11_LCD = myLCD(size)
        self.u12_LCD = myLCD(size) 
        self.xcel_LCD = myLCD(size)
        self.teledyne_LCD = myLCD(size)
        self.posifa_LCD = myLCD(size)
           
        self.fan_slider = QSlider()
        self.fan_slider.setOrientation(Qt.Horizontal)
        self.fan_slider.setMaximum(100)
        self.fan_slider.setMinimum(0)
        self.fan_slider.setTickInterval(10)
        
        self.grid.addWidget(self.label1, 1,1, Qt.AlignLeft)
        self.grid.addWidget(self.label2, 2,1, Qt.AlignLeft)
        self.grid.addWidget(self.label3, 3,1, Qt.AlignLeft)
        self.grid.addWidget(self.label4, 4,1, Qt.AlignLeft)
        self.grid.addWidget(self.label5, 5,1, Qt.AlignLeft)
        self.grid.addWidget(self.label6, 6,1, Qt.AlignLeft)
        self.grid.addWidget(self.label7, 7,1, Qt.AlignLeft)
        self.grid.addWidget(self.label8, 8,1, Qt.AlignLeft)        
        self.grid.addWidget(self.label9, 7,3, Qt.AlignLeft)
        self.grid.addWidget(self.label10, 4,3, Qt.AlignLeft)
        self.grid.addWidget(self.label11, 5, 3, Qt.AlignLeft)
        self.grid.addWidget(self.label12, 1,3, Qt.AlignLeft)
        self.grid.addWidget(self.label13, 2,3, Qt.AlignLeft)      
        self.grid.addWidget(self.label14, 3,3, Qt.AlignLeft)
        self.grid.addWidget(self.label15, 6,3, Qt.AlignLeft)
        self.grid.addWidget(self.GDA_LCD, 1,2, Qt.AlignLeft)
        self.grid.addWidget(self.GDB_LCD, 2,2, Qt.AlignLeft)
        self.grid.addWidget(self.GDC_LCD, 3,2, Qt.AlignLeft)
        self.grid.addWidget(self.HVDC_LCD, 4,2, Qt.AlignLeft)
        self.grid.addWidget(self.PWR_LCD, 5,2, Qt.AlignLeft)
        self.grid.addWidget(self.topcapADC_LCD, 6,2, Qt.AlignLeft)
        self.grid.addWidget(self.topcapIMU_LCD, 7,2, Qt.AlignLeft)
        
        self.grid.addWidget(self.MG_LCD, 8,2, Qt.AlignLeft)
        self.grid.addWidget(self.ARM_LCD, 7,4, Qt.AlignLeft)        
        self.grid.addWidget(self.u11_LCD, 4,4, Qt.AlignLeft)
        self.grid.addWidget(self.u12_LCD, 5,4, Qt.AlignLeft)
        self.grid.addWidget(self.teledyne_LCD, 1,4, Qt.AlignLeft)
        self.grid.addWidget(self.posifa_LCD, 2,4, Qt.AlignLeft)          
         
        self.grid.addWidget(self.fan_slider,3,4, Qt.AlignLeft)
        self.grid.addWidget(self.xcel_LCD, 6,4, Qt.AlignLeft)
        self.grid.setColumnStretch(0,0)
        self.grid.setColumnStretch(1,0)
        self.grid.setColumnStretch(2,0)
        self.grid.setColumnStretch(3,0)
        self.grid.setColumnStretch(4,2)
        self.insertStretch(-1)
        
class Communications_Layout(QVBoxLayout):
    def __init__(self):
        super().__init__()
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)
        
        self.top_hbox = QHBoxLayout()
        self.top_hbox.addWidget(self.AKlabel, Qt.AlignLeft)
        self.top_hbox.addWidget(myLabel(size, '   Communications ', 24))
        self.top_hbox.insertStretch(-1)
        self.grid = QGridLayout()
        self.addLayout(self.top_hbox)
        self.addLayout(self.grid)
        
        self.ComPortLabel = myLabel(size, 'Com Port', 20)
        self.ComPortEdit = myLineEdit(size)
        self.ComPortEdit.setValidator(QIntValidator(0,99))
        
        self.ComSnip = QPixmap('com_port.png')#).scaled(QSize(900,1000),Qt.IgnoreAspectRatio)
        self.ComHelper = QLabel()
        self.ComHelper.setPixmap(self.ComSnip)
        
        self.serial_connect_button = myButton2('Connect') 
        self.serial_disconnect_button = myButton2('Disconnect')
        self.SPI_label = myLabel(size, 'SPI Bus', 20)
        self.u12_label = myLabel(size, 'U12', 18)
        self.u12_connect_button = myButton(QIcon('connect2.png'), '', QSize(120,70), QSize(130,70))
        self.u12_disconnect_button = myButton(QIcon('disconnect.png'), '', QSize(120,70), QSize(130,70))
        self.u11_label = myLabel(size, 'U11', 18)
        self.u11_connect_button = myButton(QIcon('connect2.png'), '', QSize(120,70), QSize(130,70))
        self.u11_disconnect_button = myButton(QIcon('disconnect.png'), '', QSize(120,70), QSize(130,70))
        self.I2C_label = myLabel(size, 'I2C Bus', 20)
        self.adc_label = myLabel(size, 'ADC128D818', 18)
        self.imu_label = myLabel(size, 'MPU9250', 18)
        self.adc_connect_button = myButton(QIcon('connect2.png'), '', QSize(120,70), QSize(140,90))
        self.mpu_connect_button = myButton(QIcon('connect2.png'), '', QSize(120,70), QSize(140,90)) 
        self.adc_disconnect_button = myButton(QIcon('disconnect.png'), '', QSize(120,70), QSize(140,90))
        self.mpu_disconnect_button = myButton(QIcon('disconnect.png'), '', QSize(120,70), QSize(140,90))
        
        self.grid.addWidget(self.ComPortLabel,0,0,Qt.AlignCenter)
        self.grid.addWidget(self.ComPortEdit, 0, 1, Qt.AlignCenter)
        self.grid.addWidget(self.serial_connect_button, 0,2, Qt.AlignCenter)
        self.grid.addWidget(self.serial_disconnect_button,0,3, Qt.AlignCenter)
        self.grid.addWidget(self.ComHelper, 0,5,5,3,Qt.AlignLeft)
        
        self.grid.addWidget(self.SPI_label,1,0,Qt.AlignLeft)
        self.grid.addWidget(self.u12_label,2,0,Qt.AlignLeft)
        self.grid.addWidget(self.u11_label,3,0,Qt.AlignLeft)
        self.grid.addWidget(self.I2C_label,4,0,Qt.AlignLeft)
        self.grid.addWidget(self.adc_label,5,0,Qt.AlignLeft)
        self.grid.addWidget(self.imu_label,6,0,Qt.AlignLeft)
        self.grid.addWidget(self.u12_connect_button,2,1, Qt.AlignCenter)
        self.grid.addWidget(self.u12_disconnect_button,2,2, Qt.AlignCenter)
        self.grid.addWidget(self.u11_connect_button,3,1, Qt.AlignCenter)
        self.grid.addWidget(self.u11_disconnect_button,3,2, Qt.AlignCenter)    
        self.grid.addWidget(self.adc_connect_button,5,1, Qt.AlignCenter)
        self.grid.addWidget(self.adc_disconnect_button,5,2, Qt.AlignCenter)
        self.grid.addWidget(self.mpu_connect_button,6,1, Qt.AlignCenter)
        self.grid.addWidget(self.mpu_disconnect_button,6,2, Qt.AlignCenter) 
        self.grid.setColumnStretch(1,0)
        self.grid.setColumnStretch(2,0)
        self.grid.setColumnStretch(3,0)
        self.grid.setColumnStretch(4,0)
        self.grid.setColumnStretch(5,2)
        #self.grid.setColumnStretch(6,4)
        
        self.insertStretch(-1)


class Speed_Layout(QVBoxLayout):
    def __init__(self):
        super().__init__()
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)
        
        self.top_hbox = QHBoxLayout()
        self.top_hbox.addWidget(self.AKlabel, Qt.AlignLeft)
        self.top_hbox.addWidget(myLabel(size, '   Speed ', 24))
        self.top_hbox.insertStretch(-1)
        self.grid = QGridLayout()
        self.addLayout(self.top_hbox)
        self.addLayout(self.grid)