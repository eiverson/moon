# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 08:05:32 2018
@author: Erik


"""

#from HardwareGUI import HardwareGUI, myWindow
#import sys
#from PyQt5.QtWidgets import QApplication
import PyCmdMessenger
import time
from PyQt5.QtCore import QObject, pyqtSignal, QTimer, QThread, QEventLoop


'''
Demonstrate communication is working by querying the arduino
for some values, and print out the result. 
'''    
arduino = PyCmdMessenger.ArduinoBoard('COM25',
         baud_rate=115200,
         timeout=1.0,
         settle_time=2.0,
         enable_dtr=False,
         int_bytes=4,
         long_bytes=4,
         float_bytes=4,
         double_bytes=8) 
# List of commands and their associated argument formats. These must be in the
# same order as in the sketch.
commands = [["kCommError",""],
            ["kComment",""],
            ["kAcknowledge","s"],
            ["kAreYouReady","si"],
            ["kError","s"],
            ["kAskUsIfReady","s"],
            ["kYouAreReady","s"],
            ["askForForce",""],
            ["ForceFeedback", "I"], #just reading ADC128D818, not analog off board
            #["ForceCommand","d"], 
            #["error","s"],
            ["XAccel", "I"],
            ["YAccel", "I"],
            ["askForPressure",""],
            ["pressure_vals", "IIII"],
            ["LowSpeed", "I"],
            ["askForSpeed", ""],
            ["Speed", "I"],
            ["CmdTestToggle", ""],
            ["askForTemps",""],
            ["temperatures","d"]]
            #["CheckFault", ""]]
'''
This class wraps the PyCmdMessenger, allowing it to leverage signals and slots 
'''      
class CommLink(QThread):
    
    new_p1 = pyqtSignal(float)
    new_p2 = pyqtSignal(float)
    new_p3 = pyqtSignal(float)
    new_p4 = pyqtSignal(float)
    new_p5 = pyqtSignal(float)
    new_p6 = pyqtSignal(float)
    new_p7 = pyqtSignal(float)
    new_f1 = pyqtSignal(float)
    new_f2 = pyqtSignal(float)
    new_f3 = pyqtSignal(float)
    new_f4 = pyqtSignal(float)
    new_s1 = pyqtSignal(float)
    new_s2 = pyqtSignal(float)
    new_t1 = pyqtSignal(float)
    
    
    def __init__(self, *args, **kwargs):
        QThread.__init__(self)
        #super().__init__()
        self.myPyCmdMessenger = args[0]
        self.timer = QTimer()
        self.timer.setInterval(500) #mSec
        self.timer.moveToThread(self)
        self.timer.timeout.connect(self.update_request)
        
    def run(self):
        self.timer.start()
        loop = QEventLoop()
        loop.exec_()
        
    def update_request(self):
        '''
        fetch new values from PyCmdMessenger. 
        emit the signals that are connected to the GUI Widgets' slots 
        '''
        self.myPyCmdMessenger.send("askForPressure")
        vals = self.myPyCmdMessenger.receive()[1]
        #Vsns:
        Isns = (vals[1] - vals[0])*(5/4096)/10*1000
        Iref = (vals[3] - vals[2])*(5/4096)/10*1000
        Vsns = vals[0]*5/4096
        Vref = vals[3]*5/4096
        self.new_p1.emit(Vsns)
        self.new_p2.emit(Isns)
        Rsns = Vsns/(Isns/1000)
        self.new_p3.emit(Rsns)
        self.new_p4.emit(Vref)
        self.new_p5.emit(Iref)
        Rref = Vref/(Iref/1000)
        self.new_p6.emit(Rref)
        pressure = 10**((7.05 - Rsns / Rref)/.05)
        if pressure < 1000:
            self.new_p7.emit(pressure)
        else:
            self.new_p7.emit(1000)
        
        self.myPyCmdMessenger.send('askForForce')
        vals = self.myPyCmdMessenger.receive()[1]
        force_bits = vals[0]
        #emit force
        self.new_f1.emit(force_bits*(5/4096)*(1000/.014574)*(1/100))
        #emit LCA out
        self.new_f2.emit(force_bits*5/4096)
        #emit Bridge out
        self.new_f3.emit(force_bits*(5/4096)*(1/100))
        
        #Speed Quantity Update
        self.myPyCmdMessenger.send("askForSpeed")
        vals = self.myPyCmdMessenger.receive()[1]
        speed_bits = vals[0]
        self.new_s1.emit(speed_bits*(5/4096)*(110/5))
        self.new_s2.emit(speed_bits*5/4096)
        
        #temperature quantities
        self.myPyCmdMessenger.send("askForTemps")
        vals = self.myPyCmdMessenger.receive()[1]
        self.new_t1.emit(vals[0])

    def on_Test_Toggle(self):
        self.myPyCmdMessenger.send("CmdTestToggle")
        time.sleep(.2)
        
        
if __name__ == '__main__':                
    # Initialize the messenger
    c = PyCmdMessenger.CmdMessenger(arduino,commands)
    
    
    # Check connection
    c.send("kAreYouReady")
 
    # Clear welcome message etc. from buffer
    i = 0
    success = False
    while i < 3:
        value = c.receive()
        time.sleep(0.2)
        if value != None:
            success = True
        i += 1
  
    # Make sure we're connected 
    if not success:
        err = "Could not connect."
        raise Exception(err)

    '''
    c.send("askForPressure")
    vals = c.receive()[1]
    p1 = vals[0] 
    p2 = vals[1]
    p3 = vals[2]
    p4 = vals[3]
    print("Pressure Quantities:")
    print(vals)
    print(type(vals))
    print('p1 = {:4.3f}, p2 = {:4.3f}, p3 = {:4.3f}, p4 = {:4.3f}'.format(p1*5/4096,p2*5/4096,p3*5/4096,p4*5/4096))
    
    c.send("askForPressure")
    vals = c.receive()[1]
    #Vsns:
    Isns = (vals[1] - vals[0])*(5/4096)/10
    Iref = (vals[3] - vals[2])*(5/4096)/10
    Vsns = vals[0]*5/4096
    Vref = vals[3]*5/4096
    print('Vsns:')
    print(Vsns)
    print('Isns:')
    print(Isns)
    print('Rsns:')
    print(Vsns/Isns)
    print(Vref)
    print(Iref)
    print(Vref/Iref)
    
    c.send("askForSpeed")
    print()
    val = c.receive()
    speed = val[1][0]*(5.0/4096)*(110.0/5.0);
    print('speed = {:3.1f}'.format(speed))
    c.send("askForForce")
    force = c.receive()[1][0]*(5.0/4096)*(1000/.015)*(1/100)
    print('force = {:4.1f}'.format(force))
    print("toggling force bit")
    c.send("CmdTestToggle")
    time.sleep(1)
    c.send("askForSpeed")
    speed = c.receive()[1][0]*(5.0/4096)*(110.0/5.0);
    print('speed = {:3.1f}'.format(speed))
    c.send("askForForce")
    force = c.receive()[1][0]*(5.0/4096)*(1000/.015)*(1/100)
    print('force = {:4.1f}'.format(force))
    '''
    print('that is all for now')
    
    