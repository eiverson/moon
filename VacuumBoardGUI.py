# -*- coding: utf-8 -*-
"""
Created on Sun Jan  7 16:20:35 2018

@author: Erik
"""

import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QMainWindow, QAction, qApp,
    QLCDNumber, QGridLayout, QLabel, QVBoxLayout, QHBoxLayout, QSizePolicy, QScrollArea, QLayout)
from PyQt5.QtGui import QIcon, QFont, QColor, QPalette, QPixmap
from PyQt5.QtCore import Qt, QSize
from scope_ex import Scope
import matplotlib.animation as animation
'''
agg = anti grain geometry
Qt5Agg: agg rendering in a QT5 canvas 
'''
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt


class myLCD(QLCDNumber):
    def __init__(self, size):
        super().__init__()
        self.setSizePolicy(size)
        self.setSmallDecimalPoint(True)
       
    def sizeHint(self):
        return QSize(200,75)

class myLabel(QLabel):
    def __init__(self, size, text):
        super().__init__(text)
        self.setSizePolicy(size)
        self.setFont(QFont("Times",22))
        self.setAlignment(Qt.AlignCenter)
        
    def sizeHint(self):
        return QSize(200, 75)
   
class myWindow(QWidget):
    def __init__(self, scope_canvas):
        super().__init__()
        self.scope_canvas = scope_canvas
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png').scaled(QSize(200,75),Qt.KeepAspectRatio)
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setFixedSize(200,75)
                
        my_yellow = QColor.fromHsl(35, 255, 153, 128 )
        dark_blue = QColor.fromHsl(211, 196, 38, 255)
        self.setAutoFillBackground(True)
        palette = self.palette()
        palette.setColor(QPalette.Window, dark_blue)
        palette.setColor(QPalette.WindowText, my_yellow)
        self.setPalette(palette)
        
        #pressure quantities
        self.pressure_label = myLabel(size, 'PRESSURE DATA')
        self.pressure_label.setFont(QFont('Times', 28))
        self.lcd_label1 = myLabel(size, 'Vsns [V]')
        self.data_lcd1 = myLCD(size)
        self.lcd_label2 = myLabel(size,'Isns [mA]')
        self.data_lcd2 = myLCD(size)
        self.lcd_label3 = myLabel(size, 'Rsns [ohm]')
        self.data_lcd3 = myLCD(size)
        self.lcd_label4 = myLabel(size, 'Vref [V]')
        self.data_lcd4 = myLCD(size)
        self.lcd_label5 = myLabel(size, 'Iref [mA]')
        self.data_lcd5 = myLCD(size)
        self.lcd_label6 = myLabel(size, 'Rref [ohm]')
        self.data_lcd6 = myLCD(size)
        self.lcd_label16 = myLabel(size, 'Pressure')
        self.data_lcd16 = myLCD(size)        
        self.pressure_layout = QGridLayout()
          #add pressure widgets to pressure grid
        self.pressure_layout.addWidget(self.pressure_label,0,0,1,-1)
        self.pressure_layout.addWidget(self.lcd_label1,1,0)
        self.pressure_layout.addWidget(self.data_lcd1,1,1)
        self.pressure_layout.addWidget(self.lcd_label2,2,0)
        self.pressure_layout.addWidget(self.data_lcd2,2,1)       
        self.pressure_layout.addWidget(self.lcd_label3,3,0)
        self.pressure_layout.addWidget(self.data_lcd3,3,1)
        self.pressure_layout.addWidget(self.lcd_label4,4,0)
        self.pressure_layout.addWidget(self.data_lcd4,4,1)
        self.pressure_layout.addWidget(self.lcd_label5,5,0)
        self.pressure_layout.addWidget(self.data_lcd5,5,1)
        self.pressure_layout.addWidget(self.lcd_label6,6,0)
        self.pressure_layout.addWidget(self.data_lcd6,6,1)
        self.pressure_layout.addWidget(self.lcd_label16, 7, 0)
        self.pressure_layout.addWidget(self.data_lcd16,7,1)        
        
        self.pressure_layout.addWidget(self.scope_canvas, 8, 0, 2,2)
        
        
        #force quantities
        self.force_grid_parent = QVBoxLayout()
        self.force_grid = QGridLayout()
        self.force_label = myLabel(size, 'FORCE DATA')
        self.force_label.setFont(QFont('Times', 28))
        self.lcd_label7 = myLabel(size, 'Force [lbs]')
        self.data_lcd7 = myLCD(size)
        self.lcd_label8 = myLabel(size,'LCA Vout [V]')
        self.data_lcd8 = myLCD(size)
        self.lcd_label9 = myLabel(size, 'Bridge Vout')
        self.data_lcd9 = myLCD(size)
        #add force widgets to grid
        self.force_grid.addWidget(self.force_label,0,0,1,-1)
        self.force_grid.addWidget(self.lcd_label7,1,0)
        self.force_grid.addWidget(self.data_lcd7,1,1)
        self.force_grid.addWidget(self.lcd_label8,2,0)
        self.force_grid.addWidget(self.data_lcd8,2,1)
        self.force_grid.addWidget(self.lcd_label9,3,0)
        self.force_grid.addWidget(self.data_lcd9,3,1)        
        self.force_grid_parent.addLayout(self.force_grid)
        self.force_grid_parent.insertStretch(-1)
                
        #temperature
        self.temp_grid_parent = QVBoxLayout()
        self.temp_grid = QGridLayout()
        self.temp_label = myLabel(size, 'TEMP DATA')
        self.temp_label.setFont(QFont('Times', 28))
        self.lcd_label10 = myLabel(size, 'ADC Temp [C]')
        self.data_lcd10 = myLCD(size)
        self.lcd_label11 = myLabel(size, 'IMU Temp [C]')
        self.data_lcd11 = myLCD(size)
        self.lcd_label12 = myLabel(size, 'Teensy Temp [C]')
        self.data_lcd12 = myLCD(size)
        
        self.temp_grid.addWidget(self.temp_label,0,0,1,-1)
        self.temp_grid.addWidget(self.lcd_label10,1,0)
        self.temp_grid.addWidget(self.data_lcd10,1,1)
        self.temp_grid.addWidget(self.lcd_label11,2,0)
        self.temp_grid.addWidget(self.data_lcd11,2,1)
        self.temp_grid.addWidget(self.lcd_label12,3,0)
        self.temp_grid.addWidget(self.data_lcd12,3,1)  
        
        self.temp_grid_parent.addLayout(self.temp_grid)
        self.temp_grid_parent.insertStretch(-1)
        
        #speed quantities
        self.speed_grid_parent = QVBoxLayout()
        self.speed_grid = QGridLayout()
        self.speed_label = myLabel(size, 'SPEED DATA')
        self.speed_label.setFont(QFont('Times', 28))
        self.lcd_label13 = myLabel(size, 'Speed [RPS]')
        self.data_lcd13 = myLCD(size)
        self.lcd_label14 = myLabel(size, 'Tach Vout [V]')
        self.data_lcd14 = myLCD(size)
        self.lcd_label15 = myLabel(size, 'Low Speed')
        self.data_lcd15 = myLCD(size)
        
        self.speed_grid.addWidget(self.speed_label,0,0,1,-1)
        self.speed_grid.addWidget(self.lcd_label13,1,0)
        self.speed_grid.addWidget(self.data_lcd13,1,1)
        self.speed_grid.addWidget(self.lcd_label14,2,0)
        self.speed_grid.addWidget(self.data_lcd14,2,1)
        self.speed_grid.addWidget(self.lcd_label15,3,0)
        self.speed_grid.addWidget(self.data_lcd15,3,1) 
        
        self.speed_grid_parent.addLayout(self.speed_grid)
        self.speed_grid_parent.insertStretch(-1)
        
        grid = QGridLayout()
        self.setLayout(grid)

        grid.addWidget(self.AKlabel, 0,0)
        grid.addLayout(self.pressure_layout, 1,0,-1,2)
        grid.addLayout(self.force_grid_parent,1, 2)
        grid.addLayout(self.temp_grid_parent, 1,3)
        grid.addLayout(self.speed_grid_parent, 1,4)
        
        self.testButton = QPushButton()
        self.testButton.setText("Test Bit")
        self.testButton.setSizePolicy(size)
        grid.addWidget(self.testButton,0,2)

class HardwareGUI(QMainWindow):
    
    def __init__(self, myWindow):
        super().__init__()
        
        '''
        scrollArea is made the central widget of this QMainWIndow, and 
        becomes a child of it. 
        scrollAreaWidgetContents is a QWidget child of scrollArea, and supposedly 
        it must be unique. It manages the scrollbars when they are needed. 
        '''
        self.scrollArea = QScrollArea()
        self.setCentralWidget(self.scrollArea)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget(self.scrollArea)
        '''Documentation states the argument of scrollArea.setWidget() must have its
        layout set already. 
        '''
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        #this next line seems to determine when the scroll bars appear
        self.scrollAreaWidgetContents.setMinimumSize(QSize(1100, 1300)) 
        
        #make myWindow a child of scrollAreaWidgetContents
        myWindow.setParent(self.scrollAreaWidgetContents)
        
        self.initUI()
        #self.arduino = arduino
        
    def initUI(self):
              
        exitAct = QAction(QIcon('exit2.png'), '&Exit', self)        
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(self.centralWidget().close)
        exitAct.triggered.connect(qApp.closeAllWindows)        
        self.statusBar()
        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)
        self.setGeometry(300, 300, 900, 700)
        self.setWindowTitle('Amber Kinetics Vacuum Board GUI')  
        self.setWindowIcon(QIcon('A.png'))
        self.show()
        
if __name__ == '__main__':
    app = QApplication(sys.argv)
    
        # a figure instance to plot on
    fig, ax = plt.subplots(facecolor='#08233F')
    
    '''
    The scope just has axes object which it updates (using the update
    function) with data from a generator
    '''
    scope = Scope(ax)
    '''
    The canvas the figure renders into. Calls the draw and 
    print fig methods, creates the renderers, etc.
    '''
    scope_canvas = FigureCanvas(fig)
    myWindow = myWindow(scope_canvas)
    ex = HardwareGUI(myWindow)
    ani = animation.FuncAnimation(fig, scope.update, scope.emitter, interval=10,
                              blit=False)
    myWindow.data_lcd1.display(7.00)
    
    sys.exit(app.exec_())
         
'''
$background: hsla(211, 77%, 14%, 1); // dark blue background, RGB 8,35,63, 0x08233F
$name-active: hsla(210, 50%, 70%, 1); // light blue text RGB 140, 179,217, 0x8CB3D9
$name-disabled: hsla(210, 50%, 70%, 0.5);
$name-passive: hsla(210, 50%, 70%, 0.15);
$value-active: hsla(35, 100%, 60%, 1); // yellow text, RGB 255, 170,51, 0xFFAA33
$value-disabled: hsla(35, 100%, 60%, 0.3);
$error-disabled: hsla(17, 90%, 45%, 0.25);
$error-active: hsl(17, 90%, 45%); // error text
$status-good: hsl(137, 70%, 52%); // green status RGB 47,218,95, 0x2FDA5F
$status-bad: hsl(0, 66%, 43%); // red status RGB 182, 37, 37, 0xB62525
'''
    