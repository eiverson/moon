# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 07:31:49 2018

@author: Erik
"""
import sys
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import PyCmdMessenger
from PyQt5.QtWidgets import QApplication
from VacuumBoardGUI import HardwareGUI, myWindow
from VacuumBoardComm import arduino, commands, CommLink
from scope_ex import Scope

cmdMessenger = PyCmdMessenger.CmdMessenger(arduino,commands)

app = QApplication(sys.argv)
    # a figure instance to plot on
fig, ax = plt.subplots()
'''
The scope just has axes object which it updates (using the update
function) with data from a generator
'''
scope = Scope(ax)
'''
The canvas the figure renders into. Calls the draw and 
print fig methods, creates the renderers, etc.
'''
scope_canvas = FigureCanvas(fig)
myWindow = myWindow(scope_canvas)
ex = HardwareGUI(myWindow)
myCommLink = CommLink(cmdMessenger)
#connect the signals to the slots
myCommLink.new_p1.connect(myWindow.data_lcd1.display)
myCommLink.new_p2.connect(myWindow.data_lcd2.display)
myCommLink.new_p3.connect(myWindow.data_lcd3.display)
myCommLink.new_p4.connect(myWindow.data_lcd4.display)
myCommLink.new_p5.connect(myWindow.data_lcd5.display)
myCommLink.new_p6.connect(myWindow.data_lcd6.display)
myCommLink.new_p7.connect(myWindow.data_lcd16.display)
myCommLink.new_f1.connect(myWindow.data_lcd7.display)
myCommLink.new_f2.connect(myWindow.data_lcd8.display)
myCommLink.new_f3.connect(myWindow.data_lcd9.display)
myCommLink.new_s1.connect(myWindow.data_lcd13.display)
myCommLink.new_s2.connect(myWindow.data_lcd14.display)
myCommLink.new_t1.connect(myWindow.data_lcd10.display)
myWindow.testButton.clicked.connect(myCommLink.on_Test_Toggle)
myCommLink.start()
ani = animation.FuncAnimation(fig, scope.update, scope.emitter, interval=10,
                          blit=False)

sys.exit(app.exec_())