# -*- coding: utf-8 -*-
"""
Created on Mon Jan 15 07:37:28 2018

@author: Erik
"""
import sys
from PyQt5.QtWidgets import (QApplication, QWidget, QSlider, QPushButton, QMainWindow, QAction, qApp,
    QLCDNumber, QGridLayout, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout, QSizePolicy, QScrollArea, QLayout)
from PyQt5.QtGui import QIcon, QColor, QPalette, QPixmap
from PyQt5.QtCore import Qt, QSize, QRect

        
class Color2(QWidget):

    def __init__(self, color, *args, **kwargs):
        super(Color2, self).__init__(*args, **kwargs)
        self.setAutoFillBackground(True)
        palette = self.palette()
        palette.setColor(QPalette.Window, QColor(color))
        self.setPalette(palette)
        self.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum))

    def sizeHint(self):
        return QSize(250,150)
      
class MainWindow(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setWindowTitle("My Awesome App")
        
        my_yellow = QColor.fromHsl(35, 255, 153, 128 )
        dark_blue = QColor.fromHsl(211, 196, 38, 255)
        self.setAutoFillBackground(True)
        palette = self.palette()
        palette.setColor(QPalette.Window, dark_blue)
        palette.setColor(QPalette.WindowText, my_yellow)
        self.setPalette(palette)
        
        size = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)        
        
        self.AKlogo = QPixmap('AK_Horizontal_FullColor_RBG.png')
        self.AKlabel = QLabel()
        self.AKlabel.setPixmap(self.AKlogo)
        self.AKlabel.setSizePolicy(size)
        
        layout1 = QHBoxLayout()
        layout1.addWidget(self.AKlabel)
        
        layout2 = QVBoxLayout()

        layout2.addWidget(Color2('brown'))
        layout2.addWidget(Color2('black'))
        #layout2.setStretch(1,2)
        layout2.addWidget(Color2('cyan'))
        layout2.insertStretch(-1)

        layout3= QVBoxLayout()
        layout3.addWidget(Color2('red'))
        layout3.addWidget(Color2('blue'))
        #layout3.addWidget(Color2('green'))
        layout3.insertStretch(-1)
        
        layout4= QVBoxLayout()
        layout4.addWidget(Color2('green'))
        layout4.addWidget(Color2('blue'))
        layout4.addWidget(Color2('brown'))
        layout4.insertStretch(-1)
        '''
        scrollArea is made the central widget of this QMainWIndow, and 
        becomes a child of it. 
        scrollAreaWidgetContents is a QWidget child of scrollArea, and supposedly 
        it must be unique. It manages the scrollbars when they are needed. 
        '''
        self.scrollArea = QScrollArea()
        self.setCentralWidget(self.scrollArea)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget(self.scrollArea)
        '''Documentation states the argument of scrollArea.setWidget() must have its
        layout set already. 
        '''
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        #this next line seems to determine when the scroll bars appear
        self.scrollAreaWidgetContents.setMinimumSize(QSize(700, 700)) 
        self.mainLayout = QGridLayout(self.scrollAreaWidgetContents)     
        #self.mainLayout.addWidget(Color('purple'))
        #self.mainLayout.addWidget(Color('yellow'))
        #elf.mainLayout.addWidget(Color('orange'))
        self.mainLayout.addLayout(layout1, 0,0, 1, -1)
        self.mainLayout.addLayout(layout2, 1,0, -1, 1)
        self.mainLayout.addLayout(layout3, 1,1, -1, 1)
        self.mainLayout.addLayout(layout4, 1,2, -1,1)
        self.show()
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_())