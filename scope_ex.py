# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 15:21:29 2018

@author: Erik
"""

import numpy as np
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from math import sin


class Scope(object):
    def __init__(self, ax, maxt=2, dt=0.02):
        self.ax = ax
        self.dt = dt
        self.maxt = maxt
        leng = int(maxt/dt)
        self.tdata = [i*dt for i in range(leng)]
        self.ydata = [0 for i in range(leng)]
        self.line = Line2D(self.tdata, self.ydata, color='#FFAA33', linewidth=3)
        self.ax.add_line(self.line)
        self.ax.set_ylim(-1.1, 1.1)
        self.ax.set_xlim(0, self.maxt)
        self.ax.patch.set_facecolor('#08233F')
        ax.set_xlabel('Time', color='#FFAA33')
        ax.set_ylabel('Pressure [mTorr]', color = '#FFAA33')
        for tl in ax.get_yticklabels():
            tl.set_color('#FFAA33')
        for tl in ax.get_xticklabels():
            tl.set_color('#FFAA33')
        for tl in ax.get_yticklines():
            tl.set_color('#FFAA33')
        for tl in ax.get_xticklines():
            tl.set_color('#FFAA33')
        self.x = 0
        

    def update(self, y):
        '''
        
        '''
        lastt = self.tdata[-1]
        self.tdata = self.tdata[1:] + [lastt + self.dt]
        self.ydata = self.ydata[1:] + [y]
        self.ax.set_xlim(self.tdata[0], self.tdata[0] + self.maxt)
        self.line.set_data(self.tdata, self.ydata)
        self.ax.figure.canvas.draw()
        return self.line,

    # pass a generator in "emitter" to produce data for the update func
    def emitter(self, p=0.04):
        'return a random value with probability p, else 0'
        while True:
            '''
            v = np.random.rand(1)
            if v > p:
                yield 0.
            else:
                yield np.random.rand(1)
            '''
            if self.x > self.maxt:
                self.x = 0
            self.x += self.dt
            yield sin(5*self.x)
                
                
if __name__ == '__main__':
    
    # Fixing random state for reproducibility
    np.random.seed(19680801)


    fig, ax = plt.subplots()
    scope = Scope(ax)
    ani = animation.FuncAnimation(fig, scope.update, scope.emitter, interval=10,
                              blit=True)
    plt.show()